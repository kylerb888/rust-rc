use std::{fmt::Display, io::{Read, Stdout, BufReader}, ops::{AddAssign, Deref}, thread, time::Duration, cell::UnsafeCell, os::unix::net::UnixStream};

use chrono::{DateTime, Local};
use crossterm::event::{Event, EventStream, KeyCode, KeyEvent, KeyModifiers};
use futures_lite::{FutureExt};
use serde::{Deserialize, Serialize};
use futures_util::{TryFutureExt, TryStreamExt, StreamExt};
use tokio::{sync::{RwLock, mpsc::{self, UnboundedSender}}, time::sleep};
use tokio_stream::wrappers::UnboundedReceiverStream;
use std::error::Error as StdError;
use tui::{
    backend::CrosstermBackend,
    layout::{Constraint, Direction, Layout},
    style::{Color, Style},
    widgets::{Block, Borders, Cell, List, ListItem, Row, Table},
    Frame, Terminal,
};

pub type TerminalFrame<'a> = Frame<'a, CrosstermBackend<Stdout>>;
pub type DynResult<T> = Result<T, Box<dyn StdError>>;

impl InvalidRequestError {
    pub const fn new(request_name: String) -> Self {
        Self(request_name)
    }
}

#[derive(Debug)]
pub struct InvalidRequestError(String);
impl Display for InvalidRequestError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Request'{}' is invalid", self.0)
    }
}

#[derive(Debug)]
pub struct MissingRequestError;
impl StdError for MissingRequestError {}

impl Display for MissingRequestError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Request is missing")
    }
}

impl StdError for InvalidRequestError {}

#[derive(Deserialize, Serialize)]
pub enum RequestType {
    Start(Vec<String>),
    Stop(Vec<String>),
    Restart(Vec<String>),
    Status(Vec<String>),
    Reload,
    Summary,
}

impl RequestType {
    pub fn try_from_iter<T: IntoIterator<Item = String>>(iter: T) -> DynResult<Self> {
        let mut terms = iter.into_iter();
        let command = match terms.next() {
            Some(command) => command,
            None => return Ok(RequestType::Summary),
        };
        let f = match &*command {
            "start" => RequestType::Start,
            "stop" => RequestType::Stop,
            "restart" => RequestType::Restart,
            "status" => RequestType::Status,
            "reload" => return Ok(RequestType::Reload),
            other => return Err(InvalidRequestError::new(other.to_owned()).into()),
        };
        Ok(f(terms.collect()))
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub enum RunningState {
    /// Service is currently running.
    Running {
        pid: u32,
        started_at: DateTime<Local>,
    },
    /// Service exited with success status.
    Exited,
    /// Service has been explicitly stopped.
    Stopped,
    /// Service is queued to be stopped.
    QueuedForStop,
    QueuedForRestart,
    /// Service has done its job and should no longer be ran.
    Done,
    /// Service is needing to start again.
    PendingStart,
    Fail(Option<u8>), // 'None' if not caused by ExitCode
}
impl Default for RunningState {
    fn default() -> Self {
        Self::PendingStart
    }
}
impl RunningState {
    pub fn color(self) -> Color {
        use RunningState::*;
        const PRIMARY_BRIGHTNESS: u8 = 225;
        const ALT_BRIGHTNESS: u8 = 100;
        const COLOR_GREEN: Color = Color::Rgb(ALT_BRIGHTNESS, PRIMARY_BRIGHTNESS, ALT_BRIGHTNESS);

        match self {
            Running { .. } => COLOR_GREEN,
            QueuedForStop  | QueuedForRestart | Exited | Done | PendingStart | Stopped => Color::Reset,
            Fail(_) => Color::Red,
        }
    }
}
impl Display for RunningState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use RunningState::*;
        match *self {
            Running { pid, started_at } => write!(
                f,
                "running for {:.0?} (pid {})",
                Local::now()
                    .signed_duration_since(started_at)
                    .to_std()
                    .unwrap(),
                pid
            ),
            QueuedForStop | QueuedForRestart | PendingStart | Exited | Done => f.write_str("exited"),
            Stopped => f.write_str("explicitly stopped"),
            Fail(Some(exit)) => write!(f, "failed (exit code {})", exit),
            Fail(None) => f.write_str("failed (internal error)"),
        }
    }
}
pub enum ToStringCacheEnum<T> {
    Source(T),
    ToString(String),
}
impl<T> ToStringCache<T> {
    pub const fn new(source: T) -> Self {
        Self {
            data: UnsafeCell::new(ToStringCacheEnum::Source(source))
        }
    }
}
impl<T: Display> Deref for ToStringCache<T> {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        use ToStringCacheEnum::*;
        match unsafe { & *self.data.get() } {
            Source(source) => {
                let to_string = source.to_string();
                let get = unsafe { &mut *self.data.get() };
                *get = ToString(to_string);
                if let ToString(out) = get {
                    out
                } else {
                    unreachable!();
                }
            },
            ToString(data) => data,
        }
    }
}
impl<T: Display> Display for ToStringCache<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let get = &**self;
        f.write_str(get)

    }
}
pub struct ToStringCache<T> {
    data: UnsafeCell<ToStringCacheEnum<T>>,
}

mod test {
    use std::{fmt::Display, time::Instant};

    use crate::ToStringCache;

    #[test]
    fn test() {
        pub struct Test {
            data: String,
        }
        impl Display for Test {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, "here is the message: {}, that is all.", self.data)
            }
        }
        let unoptimized_start = Instant::now();
        let get = Test { data: String::from("lorem ipsum and some other latin words that I don't know") };
        const LOOPS: usize = 1_000_000;
        for _ in 0..LOOPS {
            println!("{}", get.to_string());
        }
        let unoptimized_time = unoptimized_start.elapsed();
        let optimized_start = Instant::now();
        let get = ToStringCache::new(Test { data: String::from("lorem ipsum and some other latin words that I don't know") });
        for _ in 0..LOOPS {
            println!("{}", get.to_string());
        }
        let optimized_time = optimized_start.elapsed();
        println!("optimized run: {optimized_time:.2?}\nunoptimized_run: {unoptimized_time:.2?}");
        assert!(optimized_time < unoptimized_time);

    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct LogEntry {
    time: DateTime<Local>,
    contents: SystemMessage,
}
impl LogEntry {
    pub fn render(&self) -> Row<'static> {
        Row::new([
            Cell::from(self.time.naive_local().format("%x %r").to_string()),
            Cell::from(self.contents.to_string()).style(Style::default().fg(self.contents.color())),
        ])
    }
    pub fn new(contents: SystemMessage) -> Self {
        Self {
            contents,
            time: Local::now(),
        }
    }
}
#[derive(Debug, Clone, Deserialize, Serialize)]
pub enum SystemMessage {
    Stdout(String),
    Stderr(String),
    UnitNotFound(String),
    ConditionNotFound(String),
    ConditionTimedOut,
    WaitingOnService(String),
    StateChange(RunningState),
    OtherError(String),
    MaximumRestart,
}
impl SystemMessage {
    pub fn color(&self) -> Color {
        use SystemMessage::*;
        match self {
            Stdout(..) => Color::Reset,
            Stderr(..) => Color::Yellow,
            _ => Color::Red,
        }
    }
}
impl Display for SystemMessage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use SystemMessage::*;
        match self {
            Stdout(std) | Stderr(std) => f.write_str(std),
            UnitNotFound(unit) => write!(f, "unit {unit} not found"),
            ConditionNotFound(condition) => write!(f, "condition {condition} not found"),
            WaitingOnService(service) => write!(f, "waiting on service {service}"),
            StateChange(new_state) => write!(f, "state change: {new_state}"),
            OtherError(err) => write!(f, "internal error: {err}"),
            ConditionTimedOut => f.write_str("condition timed out"),
            MaximumRestart => f.write_str("reached maximum restart times (> 300), no longer restarting"),
        }
    }
}
#[derive(Debug, Default, Clone, Deserialize, Serialize)]
pub struct StdLogs {
    pub logs: Vec<LogEntry>,
}
impl StdLogs {
    pub fn append(&mut self, rhs: Vec<LogEntry>) {
        self.logs.extend(rhs);
    }
    pub fn len(&self) -> usize {
        self.logs.len()
    }
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
    pub fn render(&self, scroll: usize, height: usize) -> Table<'_> {
        let items = self
            .logs
            .iter()
            .rev()
            .skip(scroll)
            .take(height)
            .map(LogEntry::render);
        let table = Table::new(items)
            .widths(&[Constraint::Length(20), Constraint::Percentage(100)])
            .block(Block::default().title("logs").borders(Borders::ALL))
            .header(Row::new(["time", "contents"]));
        table
    }
}
#[derive(Deserialize, Serialize, Debug)]
pub struct ServiceSummary {
    pub name: String,
    pub state: RunningState,
}

impl ServiceSummary {
    pub fn render(&self, idx: usize, cursor: usize) -> Row<'_> {
        let (fg, bg) = if idx != cursor { (Color::White, Color::Reset) } else { (Color::Black, Color::Gray) };
        Row::new([
            Cell::from(&*self.name).style(Style::default().fg(fg).bg(bg)),
            Cell::from(self.state.to_string()).style(Style::default().fg(self.state.color()))
        ])
    }
    pub const fn new(name: String, state: RunningState) -> Self {
        Self { name, state }
    }
}
impl AddAssign for StdLogs {
    fn add_assign(&mut self, rhs: Self) {
        self.logs.extend(rhs.logs);
    }
}
#[derive(Deserialize, Serialize, Debug)]
pub struct ServiceInfo {
    pub summary: ServiceSummary,
    pub start_times: u32,
    pub logs: StdLogs,
}
impl ServiceInfo {
    pub fn render<'a>(&'a self, frame: &mut TerminalFrame, scroll: usize) {
        let [heading, content] = if let [heading, content] = *Layout::default()
            .direction(Direction::Vertical)
            .margin(1)
            .constraints([Constraint::Length(4), Constraint::Percentage(100)])
            .split(frame.size()) {
                [heading, content]
        } else {
            unreachable!()
        };
        let header_contents = [Row::new([
            Cell::from(self.summary.state.to_string()).style(
                Style::default()
                    .fg(self.summary.state.color()),
            ),
            Cell::from(self.start_times.to_string()),
        ])];
        let header = Table::new(header_contents)
            .header(Row::new(["state", "start times"]))
            .widths(&[Constraint::Percentage(50); 2])
            .block(
                Block::default()
                    .title(&*self.summary.name)
                    .borders(Borders::ALL),
            );
        frame.render_widget(header, heading);
        let logs = self.logs.render(scroll, content.height as usize);

        frame.render_widget(logs, content);
    }
    pub async fn new(
        summary: ServiceSummary,
        start_times: u32,
        logs: &RwLock<StdLogs>,
    ) -> ServiceInfo {
        Self {
            summary,
            start_times,
            logs: logs.read().await.to_owned(),
        }
    }
}
pub type StringResult<T> = Result<T, String>;

#[derive(Deserialize, Serialize)]
pub struct Query {
    pub request_type: RequestType,
}
impl Query {
    pub fn new(request_type: RequestType) -> Self {
        Self {
            request_type
        }
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub enum Response {
    Result(Vec<String>),
    Status(Vec<StringResult<ServiceInfo>>),
    Summary(Vec<ServiceSummary>),
}
#[derive(Deserialize, Serialize, Debug)]
pub struct Message {
    service_name: String,
    message_type: LogEntry,
}

impl Message {
    pub const fn new(service_name: String, message_type: LogEntry) -> Self { Self { service_name, message_type } }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub enum LogType {
    Stdout(Vec<LogEntry>),
    Stderr(Vec<LogEntry>),
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum MessageType {
    Log(Vec<LogEntry>),
    State(RunningState),
}
impl Default for MessageType {
    fn default() -> Self {
        Self::State(RunningState::PendingStart)
    }
}

impl MessageType {

    pub fn try_into_log(self) -> Result<Self, Self> {
        match self {
            Self::Log(_) => Ok(self),
            other => Err(other),
        }
    }
    pub fn try_into_state(self) -> Result<RunningState, Self> {
        match self {
            Self::State(state) => Ok(state),
            other => Err(other),
        }
    }
    pub fn as_state(&self) -> Option<&RunningState> {
        if let Self::State(v) = self {
            Some(v)
        } else {
            None
        }
    }
}

pub enum Update {
    Messages(Message),
    Key(Event),
    Timer,
}

impl Response {
    pub fn update(&mut self, message: Message) {
            match self {
                Self::Status(status) => {
                    let get = status.iter_mut().find(|service| service.as_ref().map_or(false, |summary| summary.summary.name == message.service_name)).unwrap().as_mut().unwrap();
                    if let SystemMessage::StateChange(state) = message.message_type.contents {
                        get.summary.state = state;
                    }
                    get.logs.logs.push(message.message_type);
                }
                Self::Summary(logs) => {
                    if let SystemMessage::StateChange(state) = message.message_type.contents {
                        let get = logs.iter_mut().find(|service| service.name == message.service_name).unwrap();
                        get.state = state;
                    }
                }
                _ => (),
            }
    }
    pub fn len(&self) -> usize {
        match self {
            Self::Result(i) => i.len(),
            Self::Status(i) => i.len(),
            Self::Summary(i) => i.len(),
        }
    }
    pub fn table_length(&self, idx: usize) -> Option<usize> {
        match self {
            Self::Status(sum) => Some(sum[idx].as_ref().ok()?.logs.len()),
            Self::Summary(sum) => Some(sum.len()),
            _ => None,
        }
    }
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
    pub async fn render(&mut self, term: &mut Terminal<CrosstermBackend<Stdout>>, reader: UnixStream) -> DynResult<Option<Query>> {
        let mut idx = 0;
        let mut scroll = 0;
        let mut cursor = 0;
        let mut stream = EventStream::new();
        let (send, recv) = mpsc::unbounded_channel();
        thread::spawn(move || message_await(reader, send));
        // let mut keys = io::stdin().keys();
        let mut recv = UnboundedReceiverStream::new(recv);
        loop {
            // term.clear();
            match self {
                Self::Result(responses) => {
                    let list: Vec<_> = responses
                        .iter()
                        .map(String::as_str)
                        .map(ListItem::new)
                        .collect();
                    let list = List::new(list);
                    term.draw(|frame| {
                        frame.render_widget(list, frame.size());
                    })?;
                    break;
                }
                Self::Status(stat) => {
                    term.draw(|frame| match &stat[idx] {
                        Ok(ok) => ok.render(frame, scroll),
                        Err(err) => frame
                            .render_widget(List::new(vec![ListItem::new(&**err)]), frame.size()),
                    })?;
                    if stat.iter().all(Result::is_err) { break }
                }
                Self::Summary(services) => {
                    let list = services.iter().skip(scroll).enumerate().map(|(idx, summary)| ServiceSummary::render(summary, idx, cursor));
                    let table = Table::new(list)
                        .widths(&[Constraint::Percentage(25), Constraint::Percentage(75)])
                        .header(Row::new(["name", "state"]))
                        .block(Block::default().title("summary").borders(Borders::ALL));
                    term.draw(|frame| {
                        frame.render_widget(table, frame.size());
                    })?;
                }
            };
            let wait = (&mut stream)
                .err_into::<Box<dyn StdError>>()
                .try_next()
                .map_ok(|i| i.map(Update::Key))
                .or(async {
                    sleep(Duration::from_secs(1)).await;
                    Ok(None)
                }).or(async {
                    // eprintln!("awaiting message");
                    let get = recv
                        .by_ref()
                        .try_next().await?
                        .unwrap();
                    Ok(Some(Update::Messages(get)))
                })
                .await?;
            match wait {
                Some(Update::Key(Event::Key(KeyEvent {
                    code: KeyCode::Left,
                    ..
                }))) if idx != 0 => idx -= 1,
                Some(Update::Key(Event::Key(KeyEvent {
                    code: KeyCode::Right,
                    ..
                }))) if self.len() - 1 > idx => {
                    idx += 1
                },
                Some(Update::Key(Event::Key(KeyEvent {
                    code: KeyCode::Up, ..
                }))) => {
                    if cursor != 0 {
                        cursor -= 1;
                    } else if scroll != 0 {
                        scroll -= 1;
                    }
                },
                Some(Update::Key(Event::Key(KeyEvent {
                    code: KeyCode::Down,
                    ..
                }))) => {
                    if let Some(length) = self.table_length(idx) {
                        
                        if self.is_status() {

                            if scroll < length {
                                scroll += 1;
                            }

                        } else {

                            let height = term.size().unwrap().height as usize - 4;

                            if scroll + cursor < length - 1 {
                                if cursor < height {
                                    cursor += 1;
                                } else {
                                    scroll += 1;
                                }
                            }

                        }
                    }
                }
                Some(Update::Key(Event::Key(KeyEvent {
                    code: KeyCode::PageUp,
                    ..
                }))) => scroll = scroll.saturating_sub(10),
                Some(Update::Key(Event::Key(KeyEvent {
                    code: KeyCode::PageDown,
                    ..
                }))) => {
                    if let Some(len) = self.table_length(idx) {
                        scroll = (scroll + 10).clamp(0, len - 1);
                    }
                }
                Some(Update::Key(Event::Key(KeyEvent {
                    code: KeyCode::End, ..
                }))) => {
                    if let Some(len) = self.table_length(idx) {
                        scroll = len.saturating_sub(10);
                    }
                }
                Some(Update::Key(Event::Key(KeyEvent {
                    code: KeyCode::Home,
                    ..
                }))) => scroll = 0,
                Some(Update::Key(Event::Key(KeyEvent {
                    code: KeyCode::Esc | KeyCode::Char('q'),
                    ..
                })))
                | Some(Update::Key(Event::Key(KeyEvent {
                    code: KeyCode::Char('c'),
                    modifiers: KeyModifiers::CONTROL,
                }))) => break,
                Some(Update::Messages(message)) => {
                    // eprintln!("Message received! {:?}", message);
                    self.update(message);
                }
                Some(Update::Key(Event::Key(KeyEvent {
                    code: KeyCode::Enter,
                    ..
                }))) => {
                    if let Response::Summary(sum) = self {
                        return Ok(Some(Query::new(RequestType::Status(vec![sum[scroll + cursor].name.to_owned()]))))
                    }
                }
            _ => (),
            }
        }
        Ok(None)
    }
    /// Returns `true` if the response is [`Result`].
    ///
    /// [`Result`]: Response::Result
    pub fn is_result(&self) -> bool {
        matches!(self, Self::Result(..))
    }

    /// Returns `true` if the response is [`Status`].
    ///
    /// [`Status`]: Response::Status
    pub fn is_status(&self) -> bool {
        matches!(self, Self::Status(..))
    }
}
fn message_await(reader: impl Read + Send + 'static, sender: UnboundedSender<bincode::Result<Message>>) {
    let mut reader = BufReader::new(reader);
    loop {
        let out = bincode::deserialize_from::<_, Message>(&mut reader);
        if sender.send(out).is_err() {
            break;
        }
    }
}
