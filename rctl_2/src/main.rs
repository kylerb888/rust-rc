use crossterm::terminal::{disable_raw_mode, enable_raw_mode};
use nix::unistd::Uid;
use rust_rc_ipc_structures::Query;
use rust_rc_ipc_structures::{RequestType, Response};
use serde::Deserializer;
use std::error::Error as StdError;
use std::io::Read;
use std::{
    borrow::Cow,
    env,
    io::{self, Stdout},
    mem,
    os::unix::net::UnixStream,
    panic, thread,
};
use tui::backend::CrosstermBackend;
use tui::Terminal;
pub struct RawModeGuard<F: Fn() -> T, T> {
    f: F,
}
impl<F: Fn() -> T + std::panic::RefUnwindSafe, T> RawModeGuard<F, T> {
    pub fn new(f: F) -> Self {
        enable_raw_mode().expect("Failed to enable raw mode");
        Self { f }
    }
    pub fn run(self) -> T {
        let out = panic::catch_unwind(&self.f);
        disable_raw_mode().expect("Failed to disable raw mode");
        out.unwrap()
    }
}
fn main() {
    RawModeGuard::new(|| {
        let socket_path = if Uid::current().is_root() {
            Cow::Borrowed("/run/rust-rc_2.sock")
        } else {
            let xdg_runtime_dir = env::var("XDG_RUNTIME_DIR").expect("XDG_RUNTIME_DIR is unset");
            Cow::Owned(format!("{xdg_runtime_dir}/rust-rc_2.sock"))
        };

        let query = RequestType::try_from_iter(env::args().skip(1)).expect("Failed to get query");
        let mut queries = vec![Query::new(query)];
        tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .unwrap()
            .block_on(async {
                while let Some(query) = queries.last_mut() {
                    let mut socket =
                        UnixStream::connect(&*socket_path).expect("Failed to connect to socket");

                    bincode::serialize_into(&mut socket, &query)
                        .expect("Failed to serialize payload");

                    let mut res: Response = bincode::deserialize_from(&mut socket)
                        .expect("Failed to deserialize response");

                    let mut terminal = init().expect("Failed to initialize terminal interface");

                    let ctx_switch = res
                        .render(&mut terminal, socket)
                        .await
                        .expect("Failed to run program");

                    if let Some(new_query) = ctx_switch {
                        queries.push(new_query);
                    } else {
                        queries.pop();
                    }
                }
            })
    })
    .run()
}

fn init() -> io::Result<Terminal<CrosstermBackend<Stdout>>> {
    let stdout = io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut get = Terminal::new(backend)?;
    get.clear()?;
    Ok(get)
}
