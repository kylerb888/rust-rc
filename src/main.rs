<<<<<<< HEAD
mod error;
mod listener;
mod service;
mod services;
use listener::{ServiceAction, ServiceResult};
use services::{InspectErr, Services};

#[macro_export]
macro_rules! uor {
    ($x:expr, $y:expr) => {
        match $x {
            Some(val) => val,
            None => return $y,
        }
    };
    ($x:expr) => {
        match $x {
            Some(val) => val,
            None => return,
        }
    };
}

#[macro_export]
macro_rules! uorr {
    ($x:expr, $y:expr, $z:expr) => {
        match $x {
            Ok(val) => val,
            Err(error) => {
                eprintln!("Error {}: {}", $z, error);
                return $y;
            }
        }
    };
    ($x:expr, $y:expr) => {
        match $x {
            Ok(val) => val,
            Err(error) => {
                eprintln!("Error: {}", error);
                return $y;
            }
        }
    };
    ($x:expr) => {
        match $x {
            Ok(val) => val,
            Err(error) => {
                eprintln!("Error: {}", error);
                return;
            }
        }
    };
}
use crate::error::Error;
use smol::LocalExecutor;
use std::process::exit;
fn main() -> Result<(), Error> {
    println!("Starting rust-rc ...");
    let ttys = Services::create_ttys();
    let res = smol::future::block_on(LocalExecutor::new().run(async {
        let config = Services::from_path("/etc/rust-rc.d/services.d/enabled.d").await;

        let mut services = match config {
            Ok(mut services) => {
                services.extend(ttys);
                services.sort_by_key(|service| service.priority);
                services
            }
            // This may fail, but it doesn't matter
            // because if you can't spawn the TTYs
            // then there is nothing you can do.
            Err(err) => {
                eprintln!("Failed to resolve service directory: {}", err);
                ttys.collect()
            }
        };
        println!("Starting processes ...");

        if let Err(errs) = services.check_all_dependencies() {
            errs.iter()
                .filter_map(|ServiceResult { name, result }| Some((name, result.as_ref().err()?)))
                .for_each(|(name, err)| {
                    eprintln!(
                        "Failed to check dependencies for service '{}': {}",
                        name, err
                    )
                });
        };

        // services
        //    .implant_conditions()
        //    .await
        //    .inspect_err(|e| eprintln!("Failed to implant conditions for services: {}", e));

        services
            .check_conditions()
            .await
            .inspect_err(|e| eprintln!("Failed to check conditions for services: {}", e));

        let conditions = services.get_conditions();

        conditions
            .start()
            .await
            .inspect_err(|e| eprintln!("Failed to start conditions for services: {}", e));

        services
            .start()
            .await
            .inspect_err(|e| eprintln!("Failed to start services: {}", e));

        let res = services.supervise().await;

        services
            .stop()
            .await
            .inspect_err(|e| eprintln!("Failed to stop services after supervision: {}", e));

        res
    }));
    match res? {
        ServiceAction::Poweroff => exit(15),
        _ => exit(1),
    }
=======
use rust_rc_ipc_structures::DynResult;

use crate::services::Services;

mod event;
mod listener;
mod service;
mod services;

fn main() {
    tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .unwrap()
        .block_on(async {
            let mut get = Services::from_default_path().await?;
            get.supervise().await;
            DynResult::Ok(())
        })
        .expect("Failed to run program.");
>>>>>>> c3ab1bc (resync)
}
