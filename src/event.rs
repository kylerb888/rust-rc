use std::{
    cell::UnsafeCell,
    ops::Deref,
    task::{Poll, Waker},
};

use futures_util::Future;
pub struct UnsafeCellGetter<T> {
    cell: UnsafeCell<T>,
}
impl<T> Deref for UnsafeCellGetter<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { &*self.cell.get() }
    }
}
pub mod cell {
    use std::{
        cell::{UnsafeCell, Cell},
        pin::Pin,
        task::{Context, Poll, Waker},
    };

    use futures_util::{Future, Stream};

    #[derive(Debug, Default)]
    pub struct EventCell<T: Clone> {
        data: UnsafeCell<T>,
        wakers: UnsafeCell<Vec<Waker>>,
        rev: Cell<usize>,
    }
    #[derive(Debug)]
    pub struct Event<'a, T: Clone> {
        cell: &'a EventCell<T>,
        ready: bool,
        after_rev: usize,
    }
    impl<'a, T: Clone> Event<'a, T> {
        pub fn new(cell: &'a EventCell<T>) -> Self {
            Self { cell, ready: false, after_rev: cell.rev.get() }
        }
    }
    impl<T: Clone + Default> Future for Event<'_, T> {
        type Output = T;

        fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> std::task::Poll<Self::Output> {
            if self.cell.rev.get() > 0 && self.cell.rev.get() > self.after_rev {
                Poll::Ready(self.cell.get())
            } else {
                unsafe { &mut *self.cell.wakers.get() }.push(cx.waker().clone());
                self.ready = true;
                Poll::Pending
            }
        }
    }
    impl<T: Default + Clone> EventCell<T> {
        /// SAFETY: This reference shall not be held across an await point.
        pub unsafe fn get_ref(&self) -> &T {
            &*self.data.get()
        }
        pub fn get(&self) -> T {
            // Since the data in the EventCell can be mutated at any given moment, we should only hand out copies of its value.
            unsafe { &*self.data.get() }.clone()
        }
        pub fn new() -> Self {
            Self {
                wakers: Default::default(),
                data: Default::default(),
                rev: Cell::new(0),
            }
        }
        pub fn event(&self) -> Event<'_, T> {
            Event::new(self)
        }
        fn notify(&self) {
            unsafe { &mut *self.wakers.get() }
                .drain(..)
                .for_each(Waker::wake);
        }
        pub fn set(&self, data: T) {
            unsafe {*self.data.get() = data };
            self.rev.set(self.rev.get() + 1);
            self.notify();
        }
    }
    mod test {
        use std::time::Duration;

        use tokio::{join, time::sleep};

        use crate::event::Yield;

        use super::EventCell;

        #[test]
        fn test() {
            tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(async {
                    let event = EventCell::new();
                    let send_fut = async {
                        for num in 0..100_000 {
                            sleep(Duration::from_secs(1)).await;
                            event.set(num);
                        }
                    };
                    let recv_fut = async {
                        for num in 0..100_000 {
                            let get = event.event().await;
                            println!("received: {get}");

                        }
                    };
                    let arb_fut = async {
                        loop {
                            Yield::new().await;
                        }
                    };
                    join!(send_fut, recv_fut, arb_fut);
                })
        }
    }
}

pub mod buffer {
    use std::{
        cell::{Cell, UnsafeCell},
        collections::{BTreeMap, BTreeSet},
        fmt::Debug,
        marker::PhantomData,
        mem,
        pin::Pin,
        task::{Context, Poll, Waker},
    };

    use futures_util::Future;

    #[derive(Debug)]
    pub struct EventBuffer<T> {
        // We have to store pointers to the listeners because
        // references would violate rust's borrow checking.
        listeners: UnsafeCell<BTreeSet<*mut PhantomData<T>>>,
        wakers: UnsafeCell<Vec<Waker>>,
    }
    impl<T> Default for EventBuffer<T> {
        fn default() -> Self {
        Self { listeners: Default::default(), wakers: Default::default() }
    }
    }
    impl<T: Clone + Debug> EventBuffer<T> {
        pub fn new() -> Self {
            Self {
                wakers: Default::default(),
                listeners: Default::default(),
            }
        }

        #[allow(clippy::mut_from_ref)]
        fn listeners_mut(&self) -> &mut BTreeSet<*mut Event<'_, T>> {
            unsafe { &mut *self.listeners.get().cast() }
        }
        fn listeners(&self) -> &BTreeSet<*mut Event<'_, T>> {
            unsafe { &*self.listeners.get().cast() }
        }
        pub fn listen(&self) -> Event<'_, T> {
            Event::new(self)
        }
        pub fn send(&self, event: T) {
            for &listener in self.listeners() {
                // SAFETY: These pointers are guaranteed to be valid in only single-threaded scenarios because no synchronization takes place.
                // The Drop implementations of the events remove themselves from the EventBuffer when they're done.
                unsafe {
                    let listener = &*listener;
                    (&mut *listener.buffered.get()).push(event.clone());
                }
            }
            self.notify();
        }
        fn notify(&self) {
            unsafe { &mut *self.wakers.get() }
                .drain(..)
                .for_each(Waker::wake);
        }
        fn register(&self, event: &Event<'_, T>, waker: &Waker) {
            if self
                .listeners_mut()
                .insert(event as *const Event<'_, T> as *mut Event<'_, T>)
            {
                unsafe { &mut *self.wakers.get() }.push(waker.clone());
            }
        }
    }
    #[derive(Debug)]
    pub struct Event<'a, T> {
        buffer: &'a EventBuffer<T>,
        buffered: UnsafeCell<Vec<T>>,
    }
    impl<'a, T: Clone + Debug> Event<'a, T> {
        pub fn new(buffer: &'a EventBuffer<T>) -> Self {
            Self {
                buffer,
                buffered: Default::default(),
            }
        }
    }
    impl<T> Drop for Event<'_, T> {
        fn drop(&mut self) {
            unsafe { &mut *self.buffer.listeners.get() }
                .remove(&(self as *mut Event<'_, T> as *mut PhantomData<T>));
        }
    }
    impl<T: Clone + Debug> Future for Event<'_, T> {
        type Output = Vec<T>;

        fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
            let buffered = unsafe { &mut *self.buffered.get() };
            if !buffered.is_empty() {
                Poll::Ready(mem::take(buffered))
            } else {
                self.buffer.register(&*self, cx.waker());
                Poll::Pending
            }
        }
    }
    mod test {
        use std::time::Duration;

        use futures_util::future;
        use tokio::{join, select, time::sleep};

        use crate::event::{buffer::EventBuffer, Yield};

        #[test]
        fn test() {
            tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(async {
                    let buffer = EventBuffer::new();

                    let run_fut = async {
                        for idx in 0..100_000 {
                            Yield::new().await;
                            buffer.send(idx);
                            buffer.send(idx);
                        }
                    };
                    let listen_fut = async {
                        for idx in 0..100_000 {
                            eprintln!("loop {idx}");
                            let res = buffer.listen().await;
                            eprintln!("got: {res:?}");
                        }
                    };

                    join!(listen_fut, run_fut);
                })
        }
    }
}
pub mod event_waiter {
    use std::{
        cell::{Cell, UnsafeCell},
        mem,
        task::{Poll, Waker},
    };

    use futures_util::Future;

    pub struct EventWaiter<T> {
        /// Multiple events may be buffered at once, which is decided if the caller calls set() multiple times before an await point returns Poll::Pending
        events: UnsafeCell<Vec<T>>,
        waker: UnsafeCell<Option<Waker>>,
    }
    impl<T> EventWaiter<T> {
        pub fn new() -> Self {
            Self {
                events: UnsafeCell::default(),
                waker: UnsafeCell::default(),
            }
        }
        /// Calling this on multiple await points is unspecified behavior
        pub fn wait(&self) -> Event<'_, T> {
            Event::new(self)
        }
        pub fn set(&self, with: T) {
            unsafe {
                { &mut *self.events.get() }.push(with);
                if let Some(waker) = { &mut *self.waker.get() }.take() {
                    waker.wake();
                }
            }
        }
        fn get(&self) -> Vec<T> {
            mem::take(unsafe { &mut *self.events.get() })
        }
        pub fn is_populated(&self) -> bool {
            !unsafe { &*self.events.get() }.is_empty()
        }
    }

    pub struct Event<'a, T> {
        waiter: &'a EventWaiter<T>,
    }
    impl<'a, T> Event<'a, T> {
        fn new(waiter: &'a EventWaiter<T>) -> Self {
            Self { waiter }
        }
    }
    impl<T> Future for Event<'_, T> {
        type Output = Vec<T>;

        fn poll(
            self: std::pin::Pin<&mut Self>,
            cx: &mut std::task::Context<'_>,
        ) -> std::task::Poll<Self::Output> {
            if self.waiter.is_populated() {
                Poll::Ready(self.waiter.get())
            } else {
                *unsafe { &mut *self.waiter.waker.get() } = Some(cx.waker().to_owned());
                Poll::Pending
            }
        }
    }
    impl<T> Drop for Event<'_, T> {
        fn drop(&mut self) {
            unsafe { &mut *self.waiter.waker.get() }.take();
        }
    }

    mod test {
        use std::time::Duration;

        use futures_util::future;
        use tokio::{select, time::sleep};

        use super::EventWaiter;

        #[test]
        fn waiter() {
            tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(async {
                    let waiter = EventWaiter::new();
                    let wait = async {
                        sleep(Duration::from_secs(2)).await;
                        waiter.set(1);
                        () = future::pending().await;
                    };
                    let recv = waiter.wait();
                    let res = select! {
                        _ = wait => unreachable!(),
                        get = recv => get,
                    };
                    assert_eq!(res, [1])
                });
        }
    }
}
/// A future in which if awaited on will instruct the executor to
/// yield the current task only once, allowing other tasks to proceed.
/// This future can effectively allow for co-operative multitasking with async/await.
pub struct Yield {
    first_run: bool,
}
impl Yield {
    pub const fn new() -> Self {
        Self { first_run: true }
    }
}
impl Future for Yield {
    type Output = ();

    fn poll(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> Poll<Self::Output> {
        if self.first_run {
            self.first_run = false;
            cx.waker().wake_by_ref();
            Poll::Pending
        } else {
            Poll::Ready(())
        }
    }
}
