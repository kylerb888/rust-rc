<<<<<<< HEAD
use crate::{
    error::{Error, Error::*, Result},
    listener::ServiceLog,
    services::{InspectErr, Services},
    uor, uorr,
};
use futures_micro::{zip, or};
use chrono::{DateTime, Local};
use futures_util::StreamExt;
use nix::{
    sys::signal::{self, Signal},
    unistd::Pid,
};
use serde::{Deserialize, Serialize};
use smol::{
    future,
    io::{AsyncBufReadExt, BufReader},
    lock::{Mutex, MutexGuard, RwLock},
    process::{Child, ChildStderr, ChildStdout, Command},
    stream, Timer,
};
use std::{
    cell::{Cell, RefCell},
    collections::HashMap,
    fmt::{Debug, Display},
    io,
    path::{Path, PathBuf},
    process::{ExitStatus, Stdio},
    rc::Rc,
    time::Duration,
};
#[derive(Serialize)]
pub(crate) struct ServiceInfo {
    name: Rc<String>,
    logs: ServiceLog,
    status: std::result::Result<RunningState, String>,
}
#[derive(Deserialize, Debug, Default, Serialize)]
pub(crate) struct Environment {
    #[serde(default, flatten)]
    pub(crate) args: HashMap<String, String>,
}
#[derive(Deserialize, Debug, Default, Serialize)]
pub(crate) struct StopCommand {
    cmd: PathBuf,
    #[serde(default)]
    args: Box<[String]>,
}
#[derive(Debug, Serialize, Clone)]
pub(crate) struct LogEntry {
    time: DateTime<Local>,
    data: Box<str>,
}
type Log = RwLock<Vec<LogEntry>>;
type StdStream<T> = RefCell<Option<BufReader<T>>>;
#[derive(Deserialize, Debug, Default)]
pub(crate) struct Service {
    #[serde(default)]
    pub(crate) args: Box<[String]>,
    pub(crate) cmd: PathBuf,
    #[serde(default)]
    pub(crate) needs: Box<[Rc<String>]>,
    #[serde(default)]
    pub(crate) no_log: bool,
    #[serde(skip)]
    pub(crate) ready: Cell<bool>,
    #[serde(default)]
    pub(crate) conditions: Box<[String]>,
    #[serde(default)]
    pub(crate) keep_alive: bool,
    #[serde(default)]
    pub(crate) env: Environment,
    #[serde(default)]
    pub(crate) stop: Option<StopCommand>,
    #[serde(skip)]
    pub(crate) wait_on: RefCell<Services>,
    #[serde(skip)]
    pub(crate) stdout: StdStream<ChildStdout>,
    #[serde(skip)]
    pub(crate) stdout_log: Log,
    #[serde(skip)]
    pub(crate) stderr: StdStream<ChildStderr>,
    #[serde(skip)]
    pub(crate) stderr_log: Log,
    #[serde(default)]
    pub(crate) service_name: Rc<String>,
    #[serde(skip)]
    pub(crate) restart_times: Cell<u16>,
    #[serde(skip)]
    pub(crate) child: Mutex<Option<Child>>,
    #[serde(default)]
    pub(crate) priority: i8,
}

/* #[derive(Debug)]
pub(crate) enum StdoutOrStderr {
    Stdout,
    Stderr,
} */
#[derive(Debug, Serialize)]
pub(crate) enum RunningState {
    StillRunning,
    ExitedSuccess,
}
impl Service {
    pub(crate) async fn enable_service<T: Display + ?Sized>(service: &T) -> Result<()> {
        let file: PathBuf = format!("/etc/rust-rc.d/services.d/{}.toml", service).into();
        if !file.is_file() {
            Err(ServicePathDoesNotExist)
        } else {
            smol::fs::unix::symlink(
                file,
                format!("/etc/rust-rc.d/services.d/enabled.d/{}.toml", service),
            )
            .await
            .map_err(IO)
        }
    }
    /// send SIGKILL to service.
    async fn kill(&self) -> Result<()> {
        let opt = &mut *self.child.lock().await;
        let child = opt.as_mut().ok_or(ChildIsGone)?;
        child.kill()?;
        opt.take();
        Ok(())
    }
    /// Set services that must be waited on and be successful before this service gets started.
    pub(crate) fn check_conditions<'a>(&self, conditions: &'a Services) -> Result<()> {
        let conditions: Services = conditions
            .iter()
            .filter(|service| self.conditions.contains(&service.service_name))
            .map(Rc::clone)
            .collect();
        *self.wait_on.try_borrow_mut()? = conditions;
        Ok(())
    }
    /// Move stdout and stderr to the central Service struct to avoid deadlock when accessing those data.
    async fn setup_std(&self) -> Result<()> {
        let stdout = async {
            let stdout = self.get_stdout().await?;
            let target = &mut *self.stdout.try_borrow_mut()?;
            *target = Some(BufReader::new(stdout));
            let out: Result<()> = Ok(());
            out
        };
        let stderr = async {
            let stderr = self.get_stderr().await?;
            let target = &mut *self.stderr.try_borrow_mut()?;
            *target = Some(BufReader::new(stderr));
            let out: Result<()> = Ok(());
            out
        };
        let (stdout, stderr) = futures_micro::zip!(stdout, stderr).await;
        stderr?;
        stdout
    }
    pub(crate) async fn track(&self) -> Result<()> {
        loop {
            let result = self.supervise().await;
            result.inspect_err(|e| eprintln!("Failed to start {}: {}", self.service_name, e));
            match result {
                Err(ChildExitFail(e)) if self.restart_times.get() > 300 => {
                    return Err(ChildExitFail(e))
                }
                Ok(()) if !self.keep_alive => break,
                Ok(()) | Err(ChildExitFail(_)) => {}
                Err(e) => return Err(e),
            };
            self.start().await?;
            Timer::after(Duration::from_millis(100)).await;
        }
        Ok(())
    }
    pub(crate) async fn log(&self) -> Result<()> {
        let stdout = async {
            let stdout = &mut *self.stdout.try_borrow_mut()?;
            let stdout = stdout.as_mut().ok_or(StdoutIsGone)?;
            let mut stdout = stdout.lines();
            while let Some(line) = stdout.next().await.transpose()? {
                let file_out = &mut *self.stdout_log.write().await;
                file_out.push(LogEntry {
                    time: Local::now(),
                    data: line.into_boxed_str(),
                });
            }
            Result::<()>::Ok(())
        };

        let stderr = async {
            let stderr = &mut *self.stderr.try_borrow_mut()?;
            let stderr = stderr.as_mut().ok_or(StderrIsGone)?;
            let mut stderr = stderr.lines();
            while let Some(line) = stderr.next().await.transpose()? {
                let file_out = &mut *self.stderr_log.write().await;
                file_out.push(LogEntry {
                    time: Local::now(),
                    data: line.into_boxed_str(),
                });
            }
            Result::<()>::Ok(())
        };

        let (stdout, stderr) = zip!(stdout, stderr).await;
        stdout?;
        stderr
    }
    /// Create Service from filesystem directory
    pub(crate) async fn from_path(service_path: impl AsRef<Path>) -> Result<Self> {
        let config = smol::fs::read_to_string(&service_path).await?;
        let mut service = toml::from_str::<Service>(&config)?;
        if service.service_name.is_empty() {
            service.service_name = Rc::new(
                service_path
                    .as_ref()
                    .file_stem()
                    .or_else(|| service.cmd.file_name())
                    .ok_or(FailedToReadDirectory)?
                    .to_string_lossy()
                    .into_owned(),
            );
        }

        Ok(service)
    }
    async fn get_child(&self) -> MutexGuard<'_, Option<Child>> {
        self.child.lock().await
    }
    /// take() stdout from child.
    async fn get_stdout(&self) -> Result<ChildStdout> {
        let out = &mut *self.child.lock().await;
        let out = out.as_mut().ok_or(ChildIsGone)?;
        out.stdout.take().ok_or(StdoutIsGone)
    }
    /// take() stderr from child.
    async fn get_stderr(&self) -> Result<ChildStderr> {
        let out = &mut *self.child.lock().await;
        let out = out.as_mut().ok_or(StderrIsGone)?;
        out.stderr.take().ok_or(StderrIsGone)
    }
    /* async fn get_std_string(
        &self,
        stdout_or_stderr: StdoutOrStderr,
    ) -> Result<String, Error> {
        let mut std = PathBuf::from("/var/log/rust-rc");
        let mut name = self.service_name.to_owned();
        if let StdoutOrStderr::Stderr = stdout_or_stderr {
            name.push_str("-err");
        }
        std.push(&name);
        let mut std = File::open(std)
            .map_err(|_| match stdout_or_stderr {
                StdoutOrStderr::Stdout => StdoutIsGone,
                StdoutOrStderr::Stderr => StderrIsGone,
            })
            .await?;
        let mut std_string = String::new();
        std.read_to_string(&mut std_string)
            .map_err(|_| LoggingFailed)
            .await?;
        Ok(std_string)
    } */
    pub(crate) fn get_status(&self) -> Result<RunningState> {
        match self
            .child
            .try_lock()
            .as_deref_mut()
            .map(Option::as_mut)
            .flatten()
        {
            Some(ok) => {
                if let Some(status) = ok.try_status()? {
                    if status.success() {
                        Ok(RunningState::ExitedSuccess)
                    } else {
                        Err(Error::ChildExitFail(status.code().unwrap_or(0)))
                    }
                } else {
                    Ok(RunningState::StillRunning)
                }
            }
            None => Ok(RunningState::StillRunning),
        }
    }
    pub(crate) async fn get_info(&self) -> Result<ServiceInfo> {
        let (stdout, stderr) =
            zip!(self.stdout_log.read(), self.stderr_log.read()).await;

        Ok(ServiceInfo {
            name: Rc::clone(&self.service_name),
            logs: ServiceLog {
                stdout: stdout.to_owned(),
                stderr: stderr.to_owned(),
            },
            status: self.get_status().map_err(|e| e.to_string()),
        })
    }
    pub(crate) fn get_unsatisfied_dependencies(&self, other_services: &Services) -> Result<()> {
        let out: Vec<_> = self
            .needs
            .iter()
            .filter(|need| {
                other_services
                    .iter()
                    .all(move |service| **need != service.service_name)
            })
            .map(Rc::clone)
            .collect();
        if out.is_empty() {
            Ok(())
        } else {
            Err(DependenciesUnsatisfied(out))
        }
    }
    pub(crate) async fn is_valid(path: impl AsRef<Path>) -> bool {
        uorr!(smol::fs::metadata(&path).await, false).is_file()
            && uor!(path.as_ref().extension(), false) == "toml"
    }
    pub(crate) async fn wait(&self) -> Result<ExitStatus> {
        self.get_child()
            .await
            .as_mut()
            .ok_or(ChildIsGone)?
            .status()
            .await
            .map_err(IO)
    }
    /// Run the service's provided stop command (if any)
    pub(crate) async fn stop_command(&self) -> Result<()> {
        let items = uor!(&self.stop, Ok(()));
        let mut command = Command::new(&items.cmd);
        let command = command.kill_on_drop(true).args(&*items.args);

        or!(
            async {
                let res = command.status().await;
                let res = res?;
                if !res.success() {
                    eprintln!(
                        "Condition for service {} failed with {}",
                        self.service_name, res
                    );
                    Err(ChildExitFail(res.code().unwrap_or(0)))
                } else {
                    Ok(())
                }
            },
            async {
                Timer::after(Duration::from_secs(30)).await;
                eprintln!("Stop command {} failed", self.service_name);
                Err(ChildFailedToWait)
            }
        )
        .await
    }
    pub(crate) async fn stop(&self) -> Result<()> {
        self.stop_command().await.inspect_err(|err| {
            eprintln!(
                "Failed to execute stop command for service {}: {}",
                self.service_name, err
            )
        });
        let opt = &mut *self.child.lock().await;
        let child = opt.as_mut().ok_or(ChildIsGone)?;
        let id = child.id() as i32;
        println!("Stopping {} ...", self.service_name);
        match signal::kill(Pid::from_raw(id), Signal::SIGTERM) {
            Ok(()) | Err(nix::Error::Sys(nix::errno::Errno::ESRCH)) => Ok(()),
            Err(other) => Err(other),
        }?;

        or!(
            async {
                let wait = child.status().await;
                wait?;
                println!("Stopped {}", self.service_name);
                Ok(())
            },
            async {
                Timer::after(Duration::from_secs(5)).await;
                println!("{} is taking a while to stop ...", self.service_name);
                Timer::after(Duration::from_secs(25)).await;
                eprintln!("Failed to stop {}, killing", self.service_name);
                self.kill().await
            }
        )
        .await
    }
    pub(crate) async fn supervise(&self) -> Result<()> {
        if !self.no_log {
            self.log().await?;
        }
        let result = self.wait().await?;
        if !result.success() {
            Err(ChildExitFail(result.code().unwrap_or(0)))
        } else {
            Ok(())
        }
    }
    pub(crate) fn create_child(&self) -> io::Result<Child> {
        Command::new(&self.cmd)
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .args(&*self.args)
            .envs(&self.env.args)
            .kill_on_drop(true)
            .spawn()
    }
    pub(crate) async fn wait_for_conditions(&self) -> Result<()> {
        let conditions = &*self.wait_on.try_borrow_mut()?;
        let mut res = stream::iter(&conditions.data)
            .map(|service| async move {
                self.ready.set(true);
                let res = async {
                    let res = service
                        .child
                        .lock()
                        .await
                        .as_mut()
                        .ok_or(ChildIsGone)?
                        .status()
                        .await?;

                    if res.success() {
                        Ok(())
                    } else {
                        Err(ConditionFailed(Rc::clone(service)))
                    }
                };
                let time = async {
                    Timer::after(Duration::from_secs(30)).await;
                    Err(ConditionTookTooLong(Rc::clone(service)))
                };
                futures_micro::or!(res, time).await
            })
            .buffer_unordered(usize::MAX);
        let next = res.next().await.unwrap_or(Ok(()));
        res.for_each(|_| future::ready(())).await;
        next
    }
    pub(crate) async fn start(&self) -> Result<()> {
        if !self.ready.get() {
            self.wait_for_conditions().await.inspect_err(|err| {
                eprintln!(
                    "Failed to wait on conditions for service {}: {} ... continuing anyways.",
                    self.service_name, err
                )
            });
        }

        {
            let mut lock = self.child.lock().await;
            let child = self.create_child()?;
            *lock = Some(child);
        }
        if !self.no_log {
            self.setup_std().await?;
        }
        self.restart_times.set(self.restart_times.get() + 1);

        Ok(())
    }
    /* async fn kill(&self) -> Result<()> {
        let child = &mut self.child.lock().await;
        let child = child
            .as_mut()
            .map_or(Err(ChildIsGone), |f| Ok(f))?;

        child.kill().await.map_err(|_| ChildExitFail)
    } */
    /* fn get_providers<'a>(&'a self, other_services: &'a Services) -> Option<Vec<&'a Service>> {
        let matches: Vec<&Service> = other_services
            .iter()
            .filter(|service| service.service_satisfies(self))
            .collect();
        match matches.is_empty() {
            true => None,
            false => Some(matches),
        }
    } */
}
impl Ord for Service {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.service_name.cmp(&other.service_name)
    }
}
impl PartialOrd for Service {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.service_name.partial_cmp(&other.service_name)
=======
use chrono::Local;
use futures_util::{
    future, ready,
    stream::{self, FuturesOrdered, FuturesUnordered},
    task::AtomicWaker,
    Future, FutureExt, Stream, StreamExt, TryStreamExt,
};
use indexmap::IndexSet;
use nix::{
    sys::signal::{kill, Signal},
    unistd::Pid, libc::pid_t,
};
use rust_rc_ipc_structures::{
    DynResult, LogEntry, Message as ListenerMessage, MessageType, RunningState, ServiceInfo,
    ServiceSummary, StdLogs, SystemMessage,
};
use rust_rc_ipc_structures::{LogType, MessageType as Message};
use serde::Deserialize;
use std::{
    borrow::Borrow,
    cell::{BorrowMutError, Cell, RefCell, RefMut, UnsafeCell},
    collections::VecDeque,
    ffi::OsStr,
    fmt::{Debug, Display},
    hash::{Hash, Hasher},
    io, mem,
    ops::{Deref, DerefMut},
    os::raw::c_int,
    path::{Path, PathBuf},
    process::Stdio,
    rc::Rc,
    task::{Poll, Waker},
    time::Duration,
};
use tokio::{
    fs::File,
    io::{AsyncReadExt, AsyncWrite, AsyncWriteExt, BufReader, AsyncBufRead},
    join,
    process::{Child, ChildStderr, ChildStdout, Command},
    select,
    sync::{
        broadcast::{Receiver, Sender},
        RwLock,
    },
    time::sleep,
    try_join,
};
use toml::Value;
use SingleThreadMutex as Mutex;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct WrappedService(pub Rc<Service>);
impl WrappedService {
    /// WrappedServices shall only have their inner contents cloned if their reference is held across an await point.
    pub fn clone_inner(&self) -> Rc<Service> {
        self.0.clone()
    }
}
impl Borrow<str> for WrappedService {
    fn borrow(&self) -> &str {
        &self.service_name
    }
}
impl WrappedService {
    pub fn new(service: Service) -> Self {
        Self(Rc::new(service))
    }
}
impl Deref for WrappedService {
    type Target = Rc<Service>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Debug)]
pub struct StdStreams {
    pub stdout: BufReader<ChildStdout>,
    pub stderr: BufReader<ChildStderr>,
}
/// ReadyChunks from the future crate will only buffer up to the capacity argument, this struct will buffer as many that are immediately ready.
pub struct DynamicReadyChunks<T, I>
where
    T: Stream<Item = I> + Unpin,
{
    stream: T,
}
impl<T, I> DynamicReadyChunks<T, I>
where
    T: Stream<Item = I> + Unpin,
{
    pub fn new(stream: T) -> Self {
        Self { stream }
    }
}
impl<T, I> Stream for DynamicReadyChunks<T, I>
where
    T: Stream<Item = I> + Unpin,
{
    type Item = Vec<I>;

    fn poll_next(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> Poll<Option<Self::Item>> {
        let mut out = Vec::new();
        loop {
            match self.stream.poll_next_unpin(cx) {
                Poll::Ready(Some(item)) => out.push(item),
                Poll::Ready(None) if out.is_empty() => break Poll::Ready(None),
                Poll::Pending if out.is_empty() => break Poll::Pending,
                Poll::Pending | Poll::Ready(None) => break Poll::Ready(Some(mem::take(&mut out))),
            }
        }
    }
}
macro_rules! do_log {
    ($stream:expr, $logs:expr, $log:ident, $sender:expr, $is_stderr:expr) => {
        DynamicReadyChunks::new(LinesStream::new($stream.lines()).map_ok(|i| LogEntry::new(SystemMessage::$is_stderr(i))))
            .then(|contents| async {
                let get = contents.into_iter().collect::<io::Result<Vec<_>>>()?;
                $sender.send(MessageType::Log($is_stderr(get.to_owned())));
                $logs.write().await.$log.extend(get);
                io::Result::Ok(())
            })
            .try_for_each(future::ok)
    };
}
impl StdStreams {
    pub async fn log_once(
        logs: &RwLock<StdLogs>,
        sender: &EventBuffer<LogEntry>,
        stream: impl AsyncBufRead + Unpin,
        log_type: fn(String) -> SystemMessage,
    ) -> io::Result<()> {
        DynamicReadyChunks::new(LinesStream::new(stream.lines()).map_ok(|i| LogEntry::new(log_type(i))))
            .then(|contents| async {
                let get = contents.into_iter().collect::<io::Result<Vec<_>>>()?; 
                for item in &get {

                    sender.send(item.to_owned());
                }
                logs.write().await.logs.extend(get);
                io::Result::Ok(())
            })
            .try_for_each(future::ok).await
    }
    pub async fn log(
        &mut self,
        logs: &RwLock<StdLogs>,
        sender: &EventBuffer<LogEntry>,
    ) -> io::Result<()> {
        let Self { stdout, stderr, .. } = self;
        let stdout_fut = Self::log_once(logs, sender, stdout, SystemMessage::Stdout);
        let stderr_fut = Self::log_once(logs, sender, stderr, SystemMessage::Stderr);
        try_join!(stdout_fut, stderr_fut)?;
        Ok(())
    }
    pub fn new(child: &mut Child) -> Self {
        Self {
            stdout: BufReader::new(child.stdout.take().unwrap()),
            stderr: BufReader::new(child.stderr.take().unwrap()),
        }
    }
}

#[derive(Debug, Default)]
pub struct ChildOutput {
    pub logs: RwLock<StdLogs>,
}
use tokio::io::AsyncBufReadExt;
use tokio_stream::wrappers::LinesStream;
/* macro_rules! update_log {
    ($stream:expr, $log:expr) => {
        let mut lines = (&mut $stream).lines();
        while let Some(line) = lines.next().now_or_never().flatten().transpose()? {
            $log.push(LogEntry::new(line));
        }
    };
} */
impl ChildOutput {
    /* pub async fn update_logs(&self) -> io::Result<()> {
        let mut borrow_mut = self.streams.lock().await;
        let streams = borrow_mut.as_mut().unwrap();
        let mut logs = self.logs.write().await;
        update_log!(streams.stdout, logs.stdout);
        update_log!(streams.stderr, logs.stderr);
        Ok(())
    } */
}
#[derive(Debug)]
pub struct ChildInfo {
    child: Child,
    pub streams: StdStreams,
}
impl Deref for ChildInfo {
    type Target = Child;

    fn deref(&self) -> &Self::Target {
        &self.child
    }
}
impl DerefMut for ChildInfo {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.child
    }
}
impl ChildInfo {
    pub fn new(mut child: Child) -> Self {
        let streams = StdStreams::new(&mut child);
        Self { child, streams }
    }
    pub async fn wait(child: &mut Child, sv: &Service) -> DynResult<()> {
        let result = child.wait().await?;
        let code: u8 = result.code().map_or(0, |i| i as u8);
        if code != 0 {
            return Err(ChildExitError::new(code).into());
        }
        let update = match sv.state.get() {
            RunningState::QueuedForRestart => RunningState::PendingStart,
            RunningState::QueuedForStop | RunningState::Running { .. } => RunningState::Exited,
            other => other,
        };
        sv.set_state(update);
        Ok(())
    }
}

#[derive(Debug, Deserialize, Default)]
pub struct CommandBase {
    pub cmd: PathBuf,
    #[serde(default)]
    pub args: Vec<String>,
}
#[derive(Debug, Deserialize, Default)]
pub struct Service {
    #[serde(flatten)]
    pub command: CommandBase,
    #[serde(default)]
    pub service_name: String,
    #[serde(default)]
    pub keep_alive: bool,
    #[serde(skip)]
    pub child: RefCell<Option<ChildInfo>>,
    #[serde(skip)]
    pub output: ChildOutput,
    #[serde(default)]
    pub conditions: Vec<String>,
    #[serde(default)]
    pub provides_units: Vec<String>,
    #[serde(default)]
    pub units: Vec<String>,
    #[serde(default, with = "tuple_vec_map")]
    pub env: Vec<(String, ToStringValue)>,
    #[serde(skip)]
    pub start_times: Cell<u32>,
    #[serde(skip)]
    pub messages: EventBuffer<LogEntry>,
    #[serde(skip)]
    pub state: EventCell<RunningState>,
    pub stop: Option<CommandBase>,
}
impl Borrow<str> for Service {
    fn borrow(&self) -> &str {
        &self.service_name
>>>>>>> c3ab1bc (resync)
    }
}
impl PartialEq for Service {
    fn eq(&self, other: &Self) -> bool {
        self.service_name == other.service_name
    }
}
impl Eq for Service {}
<<<<<<< HEAD
=======
impl Hash for Service {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.service_name.hash(state);
    }
}
use std::error::Error as StdError;

use crate::{
    event::{buffer::EventBuffer, cell::EventCell},
    services::SharedServices,
};

#[derive(Debug)]
pub struct ServiceAlreadyStoppedError(String);
impl Display for ServiceAlreadyStoppedError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "service {} has already been stopped", self.0)
    }
}
impl StdError for ServiceAlreadyStoppedError {}
impl ServiceAlreadyStoppedError {
    pub const fn new(service_name: String) -> Self {
        Self(service_name)
    }
}
impl Service {
    pub async fn wait_for_units(
        &self,
        services: &SharedServices,
    ) -> Result<(), ConditionTimedOutError> {
        let wait = self
            .units
            .iter()
            .map(|unit| services.get_providers_of_unit(unit))
            .collect::<FuturesUnordered<_>>()
            .flat_map(stream::iter)
            .filter_map(|service| async move {
                let res = services.look_up(&service).await;
                if res.is_none() {
                    self.output
                        .logs
                        .write()
                        .await
                        .logs
                        .push(LogEntry::new(SystemMessage::UnitNotFound(service)));
                }
                res
            })
            .for_each_concurrent(None, |service| async move {
                self.output
                    .logs
                    .write()
                    .await
                    .logs
                    .push(LogEntry::new(SystemMessage::WaitingOnService(service.service_name.to_owned())));
                let mut state = service.state.get();
                loop {
                    if let RunningState::Done = state {
                        break;
                    }
                    state = service.state.event().await;
                }
            });
        select! {
            _ = wait => Ok(()),
            _ = sleep(Duration::from_secs(90)) => {
                                // systemd services time out after 90 seconds, so we shall do the same.
                // ... except that services will proceed if their conditions time out
                let error = ConditionTimedOutError::new(self.service_name.to_owned());
                self.output.logs.write().await.logs.push(LogEntry::new(SystemMessage::ConditionTimedOut));
                Err(error)
            }
        }
    }
    pub async fn stream_status(
        &self,
        connection: &Mutex<impl AsyncWrite + Unpin>,
    ) -> DynResult<()> {
        loop {
            let messages = self.messages.listen().await;

            let payload = messages
                .into_iter()
                .map(|msg| ListenerMessage::new(self.service_name.to_owned(), msg))
                .map(|msg| bincode::serialize(&msg))
                .try_fold(Vec::new(), |mut acc, x| {
                    acc.extend(x?);
                    bincode::Result::Ok(acc)
                })?;

            connection.lock().await.write_all(&payload).await?;
        }
    }
    pub async fn stream_state(&self, connection: &Mutex<impl AsyncWrite + Unpin>) -> DynResult<()> {
        loop {
            let state = self.state.event().await;
            let event =
                ListenerMessage::new(self.service_name.to_owned(), LogEntry::new(SystemMessage::StateChange(state)));
            let serialized = bincode::serialize(&event)?;
            connection.lock().await.write_all(&serialized).await?;
        }
    }
    pub fn summary(&self) -> ServiceSummary {
        let Self {
            ref service_name,
            ref state,
            ..
        } = *self;
        ServiceSummary::new(service_name.to_owned(), state.get())
    }
    pub async fn status(&self) -> io::Result<ServiceInfo> {
        let Self {
            start_times,
            output,
            ..
        } = self;
        // self.output.update_logs().await?;
        Ok(ServiceInfo::new(self.summary(), start_times.get(), &output.logs).await)
    }
    pub fn clean(&self) -> Result<(), BorrowMutError> {
        self.child.try_borrow_mut()?.take();
        self.set_state(RunningState::PendingStart);
        Ok(())
    }
    pub async fn stop(&self, allow_restart: bool, with_stop_command: bool) -> DynResult<()> {
        if let Some(CommandBase { cmd, args }) = self.stop.as_ref().filter(|_| with_stop_command) {
            let code = Command::new(cmd).args(args).spawn()?.wait().await?;
            if !code.success() {
                return Err(ChildExitError::new(code.code().map_or(0, |i| i as u8)).into());
            }
        }
        let pid = if let RunningState::Running { pid, .. } = self.state.get() {
            pid
        } else {
            return Err(ServiceAlreadyStoppedError::new(self.service_name.to_owned()).into());
        };

        let new_state = if !allow_restart {
            RunningState::QueuedForStop
        } else {
            RunningState::QueuedForRestart
        };

        self.set_state(new_state);

        kill(Pid::from_raw(pid as pid_t), Signal::SIGTERM)?;

        /* BroadcastStream::new(self.messages.sender.subscribe())
            .try_filter_map(|message| {
                future::ok(message.try_into_state().ok())
        }).any(|_| future::ready(matches!(self.state.get(), Some(RunningState::Exited))))
        .map(drop)
        .map(DynResult::Ok)
        .or(async {
            sleep(Duration::from_secs(5)).await;
            Err(ChildDidntExitError::new(self.service_name.to_owned()).into())
        }).await?; */

        Ok(())
    }
    async fn create_child(&self, services: &SharedServices) -> DynResult<ChildGuard<'_>> {
        let Self {
            command: CommandBase { ref cmd, ref args },
            ref env,
            ref state,
            ..
        } = *self;
        if state.get() == RunningState::PendingStart {
            if let Err(err) = self.wait_on_conditions(services).await {
                eprintln!(
                    "Failed to wait on conditions for service {}: {err}",
                    self.service_name
                );
            }
            if let Err(err) = self.wait_for_units(services).await {
                eprintln!(
                    "Failed to wait on units for service {}: {err}",
                    self.service_name
                );
            }
        }
        self.clean()?;

        let child = Command::new(cmd)
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .args(args)
            .kill_on_drop(true)
            .envs(env.iter().map(|(key, val)| (key, val)))
            .spawn()?;
        let pid = child.id().unwrap();
        let mut lock = self.child.try_borrow_mut()?;
        *lock = Some(ChildInfo::new(child));
        self.set_state(RunningState::Running {
            pid,
            started_at: Local::now(),
        });
        Ok(ChildGuard::new(lock, self))
    }
    pub fn set_state(&self, state: RunningState) {
        if self.state.get() == state {
            return;
        }
        self.state.set(state);
    }
    pub async fn replace_child(&self, services: &SharedServices) -> DynResult<()> {
        self.create_child(services).await?;
        Ok(())
    }
    pub async fn get_or_create_child(&self, set: &SharedServices) -> DynResult<ChildGuard<'_>> {
        let borrow = self.child.try_borrow_mut()?;
        let out = if borrow.is_none() {
            drop(borrow);
            self.create_child(set).await?
        } else {
            ChildGuard::new(borrow, self)
        };
        Ok(out)
    }
    async fn handle_err(&self, err: Box<dyn StdError>) -> DynResult<()> {
        let mut system_logs = self.output.logs.write().await;
        system_logs.logs.push(LogEntry::new(SystemMessage::OtherError(err.to_string())));
        if let Some(code) = err.downcast_ref::<ChildExitError>() {
            if let RunningState::QueuedForRestart | RunningState::QueuedForStop | RunningState::PendingStart = self.state.get() {
                return Ok(());
            }
            self.set_state(RunningState::Fail(Some(code.into_inner())));
            if self.start_times.get() > 300 {
                system_logs.logs.push(LogEntry::new(SystemMessage::MaximumRestart));
                return Err((*code).into());
            } else {
                self.start_times.set(self.start_times.get() + 1);
            }
        } else {
            self.set_state(RunningState::Fail(None));
            return Err(err);
        }
        Ok(())
    }
    pub fn is_running(&self) -> bool {
        matches!(self.state.get(), RunningState::Running { .. })
    }
    pub fn has_failed(&self) -> bool {
        matches!(self.state.get(), RunningState::Fail { .. })
    }

    fn should_restart(&self) -> bool {
        let state = self.state.get();
        state == RunningState::PendingStart || (self.keep_alive && !self.is_running())
    }
    pub async fn supervise(self: Rc<Self>, set: &SharedServices) -> DynResult<()> {
        loop {
            let result = self.lap(set).await;
            if let Err(err) = result {
                self.handle_err(err).await?;
                sleep(Duration::from_millis(100)).await;
            } else if !self.should_restart() {
                self.set_state(RunningState::Done);
                break Ok(());
            }
            // self.clean().await;
        }
    }
    async fn wait_on_conditions<'a>(
        &'a self,
        services: &'a SharedServices,
    ) -> Result<(), ConditionTimedOutError> {
        let condition_future = self
            .conditions
            .iter()
            .map(|name| async move {
                let found = match services.look_up(&**name).await {
                    Some(found) => found,
                    None => {
                        self.output
                            .logs
                            .write()
                            .await
                            .logs
                            .push(LogEntry::new(SystemMessage::ConditionNotFound(name.to_owned())));
                        return;
                    }
                };
                let mut state = found.state.get();
                loop {
                    if let RunningState::Done = state {
                        break;
                    }

                    state = found.state.event().await;
                }
            })
            .collect::<FuturesUnordered<_>>()
            .for_each(future::ready);
        select! {
            _ = condition_future => Ok(()),
            _ = sleep(Duration::from_secs(90)) => {
                // systemd services time out after 90 seconds, so we shall do the same.
                // ... except that services will proceed if their conditions time out
                self.output.logs.write().await.logs.push(LogEntry::new(SystemMessage::ConditionTimedOut));
                Err(ConditionTimedOutError::new(self.service_name.to_owned()))
            }
        }
    }
    async fn lap(&self, set: &SharedServices) -> DynResult<()> {
        let mut child = self.get_or_create_child(set).await?;
        let ChildInfo { child, streams } = &mut *child;
        streams.log(&self.output.logs, &self.messages).await?;
        ChildInfo::wait(child, self).await?;
        Ok(())
    }
    pub async fn from_path(path: impl AsRef<Path>) -> DynResult<Self> {
        let path = path.as_ref();
        let mut file = File::open(path).await?;
        let mut read = String::new();
        file.read_to_string(&mut read).await?;
        let mut out: Service = toml::from_str(&read)?;
        if out.service_name.is_empty() {
            out.service_name = path
                .file_stem()
                .unwrap_or(out.command.cmd.as_os_str())
                .to_string_lossy()
                .into_owned();
        }
        Ok(out)
    }
    pub fn is_valid(path: impl AsRef<Path>) -> bool {
        let path = path.as_ref();
        path.is_file() && path.extension().map_or(false, |f| f == "toml")
    }
}
impl StdError for ChildExitError {}
#[derive(Debug, Clone, Copy)]
pub struct ChildExitError(u8);
impl ChildExitError {
    pub const fn into_inner(self) -> u8 {
        self.0
    }
}
impl Display for ChildExitError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Child has exited with error status {}",
            self.into_inner()
        )
    }
}

impl std::ops::AddAssign for ChildInfo {
    fn add_assign(&mut self, rhs: Self) {
        self.child = rhs.child;
    }
}

impl ChildExitError {
    pub const fn new(exit_code: u8) -> Self {
        Self(exit_code)
    }
}

fn deserialize_value_to_string<'de, D>(deserializer: D) -> Result<String, D::Error>
where
    D: serde::Deserializer<'de>,
{
    Value::deserialize(deserializer).map(|i| i.to_string())
}

#[derive(Debug)]
pub struct ChildDidntExitError(String);

impl ChildDidntExitError {
    pub const fn new(service_name: String) -> Self {
        Self(service_name)
    }
}

impl Display for ChildDidntExitError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "the child for service '{}' didn't exit", self.0)
    }
}

impl StdError for ChildDidntExitError {}

#[derive(Debug)]
pub struct ConditionTimedOutError(String);

impl ConditionTimedOutError {
    pub const fn new(service_name: String) -> Self {
        Self(service_name)
    }
}

impl Display for ConditionTimedOutError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "conditions for service '{}' took too long", self.0)
    }
}

impl StdError for ConditionTimedOutError {}

#[derive(Deserialize, Debug)]
pub struct ToStringValue(#[serde(deserialize_with = "deserialize_value_to_string")] String);

impl AsRef<OsStr> for ToStringValue {
    fn as_ref(&self) -> &std::ffi::OsStr {
        self.0.as_ref()
    }
}

impl Deref for ToStringValue {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl DerefMut for ToStringValue {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

/// Manipulate certain parameters of a service on drop in order to make it cancel-safe.
pub struct ChildGuard<'a> {
    child: RefMut<'a, Option<ChildInfo>>,
    service: &'a Service,
}

impl<'a> Deref for ChildGuard<'a> {
    type Target = ChildInfo;

    fn deref(&self) -> &Self::Target {
        self.child.as_ref().unwrap()
    }
}
impl DerefMut for ChildGuard<'_> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.child.as_mut().unwrap()
    }
}
impl<'a> ChildGuard<'a> {
    pub const fn new(child: RefMut<'a, Option<ChildInfo>>, service: &'a Service) -> Self {
        Self { service, child }
    }
}
impl Drop for ChildGuard<'_> {
    fn drop(&mut self) {
        self.child.take();
        if let Ok(mut logs) = self.service.output.logs.try_write() {
            logs.logs
                .push(LogEntry::new(SystemMessage::StateChange(self.service.state.get())));
        }
        let new_state = match self.service.state.get() {
            RunningState::QueuedForRestart => RunningState::PendingStart,
            RunningState::QueuedForStop | RunningState::Running { .. } => RunningState::Exited,
            other => other,
        };
        self.service.set_state(new_state);
    }
}
#[derive(Debug)]
pub struct SingleThreadMutexGuard<'a, T> {
    mutex: &'a SingleThreadMutex<T>,
}

impl<'a, T> SingleThreadMutexGuard<'a, T> {
    pub fn new(mutex: &'a SingleThreadMutex<T>) -> Self {
        mutex.locked.set(true);
        Self { mutex }
    }
}
impl<T> Deref for SingleThreadMutexGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { &*self.mutex.data.get() }
    }
}
impl<T> DerefMut for SingleThreadMutexGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.mutex.data.get() }
    }
}
#[derive(Debug, Default)]
/// Asynchronous unique-access to memory for use in single-threaded async scenarios.
pub struct SingleThreadMutex<T> {
    locked: Cell<bool>,
    data: UnsafeCell<T>,
    queue: UnsafeCell<VecDeque<Waker>>,
}
impl<T> SingleThreadMutex<T> {
    pub fn new(data: T) -> Self {
        Self {
            locked: Cell::new(false),
            data: UnsafeCell::new(data),
            queue: UnsafeCell::default(),
        }
    }
    pub fn try_lock(&self) -> Option<SingleThreadMutexGuard<'_, T>> {
        (!self.locked.get()).then(|| SingleThreadMutexGuard::new(self))
    }
    pub fn lock(&self) -> Lock<'_, T> {
        Lock::new(self)
    }
}
pub struct Lock<'a, T> {
    mutex: &'a SingleThreadMutex<T>,
}

impl<'a, T> Lock<'a, T> {
    pub const fn new(mutex: &'a SingleThreadMutex<T>) -> Self {
        Self { mutex }
    }
}
impl<'a, T> Future for Lock<'a, T> {
    type Output = SingleThreadMutexGuard<'a, T>;

    fn poll(self: std::pin::Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        self.mutex.try_lock().map_or_else(
            || {
                unsafe { &mut *self.mutex.queue.get() }.push_back(cx.waker().clone());
                Poll::Pending
            },
            Poll::Ready,
        )
    }
}
impl<T> Drop for SingleThreadMutexGuard<'_, T> {
    fn drop(&mut self) {
        self.mutex.locked.set(false);
        if let Some(waker) = unsafe { &mut *self.mutex.queue.get() }.pop_front() {
            waker.wake();
        }
    }
}
#[cfg(test)]
mod tests {
    use futures_util::StreamExt;
    use futures_util::{future, stream::FuturesUnordered};
    use tokio::join;
    use tokio::time::sleep;

    use super::SingleThreadMutex;
    use crate::event::cell::EventCell;
    use crate::event::Yield;
    use std::time::{Duration, Instant};

    #[test]
    fn event() {
        let event_cell = EventCell::new();
        let set_fut = async {
            for idx in 0..100_000 {
                event_cell.set(idx);
                Yield::new().await;
            }
        };
        let listen_fut = async {
            for _ in 0..100_000 {
                let get = event_cell.event().await;
                eprintln!("got {get}!");
            }
        };
        tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .unwrap()
            .block_on(async {
                join!(listen_fut, set_fut);
            })
    }

    #[test]
    fn mutex() {
        tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .unwrap()
            .block_on(async {
                let start = Instant::now();
                let mutex = SingleThreadMutex::new(Vec::new());
                let lock_fut = async {
                    let lock = mutex.lock().await;
                    sleep(Duration::from_secs(5)).await;
                    drop(lock);
                };
                let test_fut = (0..500)
                    .map(|num| (num, &mutex))
                    .map(|(num, mutex)| async move {
                        let push_fut = async {
                            mutex.lock().await.push(num);
                        };
                        let sleep_fut = sleep(Duration::from_secs(1));
                        join!(push_fut, sleep_fut);
                    })
                    .collect::<FuturesUnordered<_>>()
                    .for_each(future::ready);
                join!(lock_fut, test_fut);
                let get: Vec<_> = (0..500).collect();
                let lock = mutex.lock().await;
                assert_eq!(*lock, *get);
                assert!(start.elapsed() >= Duration::from_secs(5));
            })
    }
}
>>>>>>> c3ab1bc (resync)
