<<<<<<< HEAD
use crate::{
    error::{Error, Error::*},
    service::{LogEntry, RunningState, Service, ServiceInfo},
    services::{InspectErr, Services},
};
use bincode::ErrorKind;
use futures_util::StreamExt;
use serde::Serialize;
use smol::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::unix::{UnixListener, UnixStream},
    stream,
};
use std::{fmt::Display, os::unix::prelude::PermissionsExt, rc::Rc, str::FromStr, *};

impl<T, E: Display> Display for ServiceResult<T, E> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Err(err) = &self.result {
            write!(f, "{}: {}", self.name, err)?;
        }
        Ok(())
    }
}
#[derive(Serialize)]
pub(crate) struct ServiceResult<T, E> {
    pub(crate) name: Rc<String>,
    pub(crate) result: Result<T, E>,
}

const SOCK_WRITE_ERR: &str = "Failed to write_all data to socket sender";

#[derive(Serialize, Clone)]
pub(crate) struct ServiceLog {
    pub(crate) stdout: Vec<LogEntry>,
    pub(crate) stderr: Vec<LogEntry>,
}

#[derive(Serialize)]
pub(crate) struct ServiceTableEntry {
    pub(crate) name: Rc<String>,
    pub(crate) status: Result<RunningState, String>,
}
use self::ServiceAction::*;
pub(crate) enum ServiceAction {
    Continue,
    ChangeServices(Services),
    Poweroff,
    Reboot,
}
impl ServiceAction {
    pub(crate) async fn log_all<'a>(
        services: &Services,
        query: Vec<impl Display + PartialEq<String>>,
    ) -> Vec<ServiceResult<ServiceLog, Error>> {
        let res = stream::iter(query)
            .map(|search| async move {
                ServiceResult {
                    name: Rc::new(search.to_string()),
                    result: Self::get_log(services, &search).await,
                }
            })
            .buffer_unordered(usize::MAX)
            .collect::<Vec<_>>()
            .await;
        res
    }
    pub(crate) async fn disable_all(query: Vec<impl Display>) -> Vec<ServiceResult<(), io::Error>> {
        stream::iter(query)
            .map(|search| async move {
                ServiceResult {
                    name: Rc::new(search.to_string()),
                    result: smol::fs::remove_file(format!(
                        "/etc/rust-rc.d/services.d/{}.toml",
                        search
                    ))
                    .await,
                }
            })
            .buffer_unordered(usize::MAX)
            .collect::<Vec<_>>()
            .await
    }
    pub(crate) async fn stop_all(
        services: &Services,
        query: Vec<&(impl PartialEq<String> + Display + ?Sized)>,
    ) -> (Result<Self, Error>, Vec<ServiceResult<(), Error>>) {
        let (stop, errs) = stream::iter(query)
            .map(|search| async move { (search, Self::stop_service(services, search).await) })
            .buffer_unordered(usize::MAX)
            .fold(
                (Services::new(), Vec::new()),
                |(mut stop, mut errs), (name, res)| {
                    future::ready({
                        match res {
                            Ok(res) => stop.push(Rc::clone(res)),
                            Err(err) => {
                                errs.push(ServiceResult {
                                    name: Rc::new(name.to_string()),
                                    result: Err(err),
                                });
                            }
                        };
                        (stop, errs)
                    })
                },
            )
            .await;
        let cloned = services
            .iter()
            .filter(|service| !stop.contains(service))
            .map(Rc::clone)
            .collect();
        (Ok(ChangeServices(cloned)), errs)
    }
    pub(crate) async fn restart_all(
        services: &Services,
        query: Vec<impl PartialEq<String> + Display>,
    ) -> Vec<ServiceResult<(), Error>> {
        stream::iter(query)
            .map(|search| async move {
                ServiceResult {
                    name: Rc::new(search.to_string()),
                    result: Self::restart_service(services, &search).await,
                }
            })
            .buffer_unordered(usize::MAX)
            .collect::<Vec<_>>()
            .await
    }
    pub(crate) async fn enable_all<I>(query: Vec<&I>) -> Vec<ServiceResult<(), Error>>
    where
        I: ?Sized + Display,
    {
        stream::iter(query)
            .map(|search| async move {
                ServiceResult {
                    name: Rc::new(search.to_string()),
                    result: Service::enable_service(search).await,
                }
            })
            .buffer_unordered(usize::MAX)
            .collect::<Vec<_>>()
            .await
    }
    async fn get_log(
        services: &Services,
        service: &impl PartialEq<String>,
    ) -> Result<ServiceLog, Error> {
        let item = Self::find_service(services, service)?;
        let (stdout, stderr) =
            futures_micro::zip!(item.stdout_log.read(), item.stderr_log.read()).await;
        Ok(ServiceLog {
            stdout: stdout.to_owned(),
            stderr: stderr.to_owned(),
        })
    }
    pub(crate) async fn all_status<'a>(
        search: Vec<impl PartialEq<String> + Display>,
        services: &Services,
    ) -> Vec<ServiceResult<ServiceInfo, Error>> {
        stream::iter(search)
            .map(|query| async move {
                let service = match Self::find_service(services, &query) {
                    Ok(ok) => ok,
                    Err(err) => {
                        return ServiceResult {
                            name: Rc::new(query.to_string()),
                            result: Err(err),
                        }
                    }
                };
                ServiceResult {
                    name: Rc::new(query.to_string()),
                    result: service.get_info().await,
                }
            })
            .buffer_unordered(usize::MAX)
            .collect::<Vec<_>>()
            .await
    }
    /* pub(crate) async fn start_all<'a, T, I>(search: T, services: &Services, stream: &mut UnixStream) -> Result<Services, Error>
    where
        T: IntoIterator<Item=I>,
        I: AsRef<str> + Display + std::cmp::PartialEq
    {
        let mut host = Services::from_path("/etc/rust-rc.d/services.d/enabled.d").await?;
        let mut current = services.cloned();
        for query in search {
            let found = host.iter().position(|s| query.as_ref() == *s.service_name);
            if let Some(index) = found {
                let item = host.swap_remove(index);
                current.push(item);
            } else {
                stream.write_all(format!("Service '{}' not found.", query).as_bytes()).await?;
            }
        }
        Ok(current)
    } */

    pub(crate) async fn start_all<'a, I>(
        search: Vec<I>,
        services: &Services,
    ) -> (Result<Self, Error>, Vec<ServiceResult<(), Error>>)
    where
        I: Display,
    {
        let mut current = services.cloned();
        let (add, mut errs) = stream::iter(search)
            .map(|query| async move {
                let path = format!("/etc/rust-rc.d/services.d/{}.toml", query);
                (query, Service::from_path(&path).await)
            })
            .buffer_unordered(usize::MAX)
            .fold(
                (Services::new(), Vec::new()),
                |(mut add, mut errs), (name, res)| {
                    future::ready({
                        match res {
                            Ok(found) => add.push(Rc::new(found)),
                            Err(err) => {
                                errs.push(ServiceResult {
                                    name: Rc::new(name.to_string()),
                                    result: Err(err),
                                });
                            }
                        };
                        (add, errs)
                    })
                },
            )
            .await;
        current.extend(add.cloned().data);
        if let Err(err) = current.check_all_dependencies() {
            errs.extend(err);
        }
        if let Err(err) = add.start().await {
            errs.extend(Err(err))
        }
        (Ok(ChangeServices(current)), errs)
    }
    pub(crate) async fn listen(services: &Services) -> Result<Self, Error> {
        println!("Listening to socket ...");
        smol::fs::remove_file("/var/rust-rc.sock")
            .await
            .inspect_err(|_| ());
        let stream =
            UnixListener::bind("/var/rust-rc.sock").expect("Failed to start socket listener");

        let perm: Result<(), io::Error> = (|| async {
            smol::fs::symlink_metadata("/var/rust-rc.sock")
                .await?
                .permissions()
                .set_mode(0o600);
            Ok(())
        })()
        .await;

        perm.inspect_err(|e| eprintln!("Failed to setup permissions for socket listener: {}", e));
        loop {
            let (mut stream, _) = stream.accept().await?;
            match Self::handle_connection(services, &mut stream).await {
                Ok(Continue) => continue,
                Ok(other) => return Ok(other),
                Err(err) => {
                    stream
                        .write_all(err.to_string().as_bytes())
                        .await
                        .inspect_err(|e| eprintln!("{}: {}", e, SOCK_WRITE_ERR));
                    eprintln!("[RUST_RC]: Socket listener handled error: {}", err);
                }
            };
        }
    }
    pub(crate) async fn restart_service(
        services: &Services,
        search: &impl PartialEq<String>,
    ) -> Result<(), Error> {
        let found = Self::find_service(services, search)?;
        found.stop().await?;
        found.start().await
    }
    pub(crate) async fn stop_service<'a>(
        services: &'a Services,
        search: &(impl PartialEq<String> + ?Sized),
    ) -> Result<&'a Rc<Service>, Error> {
        let found = Self::find_service(services, search)?;
        found.stop().await?;
        Ok(found)
    }
    pub(crate) fn find_service<'a>(
        services: &'a Services,
        search: &(impl PartialEq<String> + ?Sized),
    ) -> Result<&'a Rc<Service>, Error> {
        services
            .iter()
            .find(|s| *search == *s.service_name)
            .ok_or(ServiceNotFound)
    }
    pub(crate) async fn handle_connection(
        services: &Services,
        stream: &mut UnixStream,
    ) -> Result<ServiceAction, Error> {
        let mut buf = [0; 256];
        let length = stream.read(&mut buf).await?;
        let data = str::from_utf8(&buf[..length])?;
        let mut segments = data.split_whitespace();
        let command = segments.next().map(QueryType::from_str).transpose()?;
        let segments: Vec<_> = segments.collect();

        let payload = match command {
            Some(Disable) => Self::disable_all(segments).await.into_binary(),
            Some(Enable) => Self::enable_all(segments).await.into_binary(),
            Some(Log) => Self::log_all(services, segments).await.into_binary(),
            Some(QueryType::Poweroff) => return Ok(Self::Poweroff),
            Some(QueryType::Reboot) => return Ok(Self::Reboot),
            Some(Restart) => Self::restart_all(services, segments).await.into_binary(),
            Some(Start) => {
                let (action, errors) = Self::start_all(segments, &services).await;
                let errors = errors.into_binary()?;
                stream.write_all(&errors).await?;
                stream.flush().await?;
                return action;
            }
            Some(Status) => Self::all_status(segments, services).await.into_binary(),
            Some(Stop) => {
                let (action, errors) = Self::stop_all(services, segments).await;
                let errors = errors.into_binary()?;
                stream.write_all(&errors).await?;
                stream.flush().await?;
                return action;
            }
            None => {
                let res: Vec<_> = services
                    .iter()
                    .map(|service| ServiceTableEntry {
                        name: Rc::clone(&service.service_name),
                        status: service.get_status().map_err(|e| e.to_string()),
                    })
                    .collect();
                bincode::serialize(&res)
            }
        }?;
        stream.flush().await?;
        stream.write_all(&payload).await?;
        Ok(Continue)
    }
}

trait IntoBinary {
    fn into_binary(self) -> Result<Vec<u8>, Box<ErrorKind>>;
}
impl<T, O, E> IntoBinary for T
where
    O: Serialize,
    E: Display,
    T: IntoIterator<Item = ServiceResult<O, E>>,
{
    fn into_binary(self) -> Result<Vec<u8>, Box<ErrorKind>> {
        let coll: Vec<_> = self
            .into_iter()
            .map(|ServiceResult { name, result }| ServiceResult {
                name,
                result: result.map_err(|e| e.to_string()), // Errors occasionally don't implement Serialize... so we'll just stringify them
            })
            .collect();
        bincode::serialize(&coll)
    }
}

use QueryType::*;
enum QueryType {
    Disable,
    Enable,
    Log,
    Poweroff,
    Reboot,
    Restart,
    Start,
    Status,
    Stop,
}

impl FromStr for QueryType {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "disable" => Disable,
            "enable" => Enable,
            "log" => Log,
            "poweroff" => Self::Poweroff,
            "reboot" => Self::Reboot,
            "restart" => Restart,
            "start" => Self::Start,
            "status" => Status,
            "stop" => Self::Stop,
            other => return Err(Error::InvalidQuery(other.to_owned())),
        })
    }
}
=======
use std::ops::Deref;
use std::rc::Rc;
use std::{error::Error as StdError, fmt::Display, io, usize};

use crate::event::event_waiter::EventWaiter;
use crate::service::Service;
use crate::service::SingleThreadMutex as Mutex;
use crate::service::WrappedService;
use crate::services::SharedServices;
use futures_util::stream::FusedStream;
use futures_util::Stream;
use futures_util::{future, stream::FuturesUnordered, StreamExt, TryStreamExt};
use indexmap::IndexSet;
use rust_rc_ipc_structures::{
    DynResult, Query, RequestType, Response, RunningState, ServiceInfo, ServiceSummary,
    StringResult,
};
use tokio::io::BufWriter;
use tokio::sync::RwLock;
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::{UnixListener, UnixStream},
};
use tokio::{join, try_join};
use tokio_stream::wrappers::UnixListenerStream;

#[derive(Debug)]
pub struct Listener {
    socket: UnixListenerStream,
}
#[derive(Debug)]
pub struct ServiceAlreadyStartedError(String);
impl Display for ServiceAlreadyStartedError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Service '{}' has already been started", self.0)
    }
}
impl StdError for ServiceAlreadyStartedError {}

impl ServiceAlreadyStartedError {
    pub const fn new(service_name: String) -> Self {
        Self(service_name)
    }
}
impl StdError for ServiceNotFoundError {}
#[derive(Debug)]
pub struct ServiceNotFoundError(String);

impl ServiceNotFoundError {
    pub const fn new(service_name: String) -> Self {
        Self(service_name)
    }
}
impl Display for ServiceNotFoundError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Service '{}' not found", self.0)
    }
}
pub enum ServiceAction {
    Start(Vec<String>),
    Continue,
    Reload,
}
impl Listener {
    pub fn new(path: &str) -> io::Result<Self> {
        Ok(Self {
            socket: UnixListenerStream::new(UnixListener::bind(path)?),
        })
    }
    async fn status(names: &[String], services: &SharedServices) -> Vec<StringResult<ServiceInfo>> {
        names
            .iter()
            .map(|name| async {
                let rw_lock_read_guard = services.read().await;
                let service = rw_lock_read_guard
                    .get(&**name)
                    .map(WrappedService::clone_inner)
                    .ok_or_else(|| ServiceNotFoundError::new(name.to_owned()))?;
                drop(rw_lock_read_guard);
                Ok(service.status().await?)
            })
            .collect::<FuturesUnordered<_>>()
            .map_err(|i: Box<dyn StdError>| i.to_string())
            .collect()
            .await
    }
    async fn process_query(
        mut connection: UnixStream,
        services: &SharedServices,
        services_base_path: &str,
    ) -> DynResult<Option<ServiceAction>> {
        let mut buf = [0_u8; 16384];
        let len = connection.read(&mut buf).await?;
        let get = &buf[..len];
        let query: Query = bincode::deserialize(get)?;
        let action = match query.request_type {
            RequestType::Start(names) => {
                let (get, errors) = services.start(&names, services_base_path).await;

                let errors = Response::Result(errors);
                let out = bincode::serialize(&errors)?;
                connection.write_all(&out).await?;

                Some(ServiceAction::Start(get))
            }
            RequestType::Stop(ref names) | RequestType::Restart(ref names) => {
                let errors = services
                    .stop(names, matches!(query.request_type, RequestType::Restart(_)))
                    .await;
                let errors = Response::Result(errors);
                let out = bincode::serialize(&errors)?;
                connection.write_all(&out).await?;
                None
            }
            RequestType::Status(names) => {
                let status = Self::status(&names, services).await;
                let status = Response::Status(status);
                let out = bincode::serialize(&status)?;
                connection.write_all(&out).await?;

                let connection = Mutex::new(connection);

                services.stream_state(&names, &connection).await?;

                None
            }
            RequestType::Summary => {
                let summary = services.summary().await;
                let summary = Response::Summary(summary);
                let out = bincode::serialize(&summary)?;
                connection.write_all(&out).await?;
                let connection = Mutex::new(connection);

                services.stream_summary(&connection).await?;
                None
            }
            RequestType::Reload => Some(ServiceAction::Reload),
        };
        Ok(action)
    }
    pub fn queries<'a>(
        &'a mut self,
        services: &'a SharedServices,
        services_base_path: &'a str,
    ) -> impl Stream<Item = ServiceAction> + 'a {
        (&mut self.socket)
            .map_ok(|i| Self::process_query(i, services, services_base_path))
            .err_into()
            .try_buffer_unordered(usize::MAX)
            .filter_map(|i| {
                future::ready(i.unwrap_or_else(|err| {
                    eprintln!("Listener request failed: {err}");
                    None
                }))
            })
    }
}

/*
struct PoolListeners<W, M> {
    pub info: BTreeMap<usize, ConnectionInfo<W, M>>,
}
impl<W, M> PoolListeners<W, M> {
    pub fn new() -> Self {
        Self {
            info: BTreeMap::new()
        }
    }
}

struct ConnectionInfo<W, M> {
    sender: Rc<Mutex<W>>,
    phantom: PhantomData<M>,
}

impl<W: AsyncWrite + Unpin, M: 'static + Serialize> ConnectionInfo<W, M> {
    async fn send_message(&self, message: &M) -> DynResult<()> {
        let serialized = serde_json::ser::to_vec(message)?;
        self.sender.lock().await.write_all(&serialized).await?;
        Ok(())
    }
    fn new(sender: Rc<Mutex<W>>) -> Self {
        Self {
            sender,
            phantom: PhantomData,
        }
    }
}

#[derive(Debug)]
pub struct AssociationNotFoundError<T>(T);

impl<T> AssociationNotFoundError<T> {
    pub const fn new(t: T) -> Self {
        Self(t)
    }
}

#[derive(Debug)]
pub struct ConnectionNotFoundError<T>(T);

impl<T: Debug + Display> StdError for ConnectionNotFoundError<T> {}

impl<T> ConnectionNotFoundError<T> {
    pub const fn new(t: T) -> Self {
        Self(t)
    }
}

impl<T: Display> Display for AssociationNotFoundError<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "association '{}' not found", self.0)
    }
}
impl<T: Display> Display for ConnectionNotFoundError<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "connection '{}' not found", self.0)
    }
}
use std::fmt::Debug;
impl<T: Debug + Display> StdError for AssociationNotFoundError<T> {}

pub struct ConnectionPool<W, T, M> {
    connections: RwLock<BTreeMap<usize, Vec<T>>>,
    associations: RwLock<HashMap<T, PoolListeners<W, M>>>,
    next_id: Cell<usize>,
}
impl<W, T, M> ConnectionPool<W, T, M>
where
    T: 'static + Eq + Hash + Clone + Debug + Display,
    M: 'static + Serialize,
    W: 'static + AsyncWrite + Unpin,
{
    pub async fn remove(&self, id: usize) -> Result<(), ConnectionNotFoundError<usize>> {
        let mut connections_guard = self.connections.write().await;
        let associations = connections_guard
            .remove(&id)
            .ok_or_else(|| ConnectionNotFoundError::new(id))?;
        let mut remove_from = self.associations.write().await;
        for association in &associations {
            // Are there any other streams associated with key?
            let pool_listeners = remove_from.get_mut(association).unwrap();
            pool_listeners.info.remove(&id);
            if pool_listeners.info.is_empty() {
                remove_from.remove(association);
            }
        }
        Ok(())
    }
    pub async fn send_message(&self, association: T, message: M, id: usize) -> DynResult<()> {
        let rw_lock_read_guard = self.associations.read().await;
        let get = rw_lock_read_guard
            .get(&association)
            .ok_or_else(|| AssociationNotFoundError::new(association.to_owned()))?;
        if *get.info.keys().next().unwrap() != id {
            return Ok(());
        }
        get.info.iter()
            .map(|(_, connection)| async {
                connection.send_message(&message).await?;
                DynResult::Ok(())
            })
            .collect::<FuturesUnordered<_>>()
            .try_for_each(future::ok)
            .await?;
        Ok(())
    }
    pub async fn contains_id(
        &self,
        id: usize,
    ) -> bool {
        self.connections.read().await.contains_key(&id)
    }
    pub async fn sole_handler(
        &self,
        id: &T,
    ) -> Option<bool> {
        Some(self.associations.read().await.get(id)?.info.len() == 1)
    }
    /// Insert a connection into the pool, and associate it with 'T'
    pub async fn insert(
        &self,
        connection: Rc<Mutex<W>>,
        associated_with: Vec<T>,
    ) -> DynResult<usize> {
        let current = self.next_id.get();
        let mut connections = self.connections.write().await;
        connections.insert(current, associated_with.clone());

        drop(connections);

        let mut associations = self.associations.write().await;
        for associated_with in associated_with {
            let get = associations.entry(associated_with).or_insert_with(PoolListeners::new);
            get.info.insert(current, ConnectionInfo::new(connection.clone()));
        }

        self.next_id.set(current + 1);
        Ok(current)
    }
    /* pub async fn run(&self) -> DynResult<()> {
        enum RecvType<M> {
            Message((Option<M>, usize)),
            NewNotifier(Pin<Box<dyn Future<Output = (Option<M>, usize)>>>),
            Done,
        }
        loop {
            let mut borrow_mut = self.tasks.borrow_mut();
            let next = borrow_mut
                .select_next_some()
                .map(RecvType::Message)
                .or(async {
                    if let Ok(recv) = self.recv.recv().await {
                        RecvType::NewNotifier(recv)
                    } else {
                        RecvType::Done
                    }
                })
                .await;
            match next {
                RecvType::Message((Some(message), id)) => {
                    let read = self.connections.read().await;
                    let take = &read[&id];
                    let serialized = serde_json::ser::to_vec(&*message)?;
                    take.stream.lock().await.write_all(&serialized).await?;
                    borrow_mut.push(Box::pin(take.clone().receive(id)));
                }
                RecvType::NewNotifier(fut) => {
                    borrow_mut.push(fut);
                }
                RecvType::Message((None, id)) => {
                    let name = self.connections.write().await.remove(&id).unwrap();
                    let mut associations = self.associations.write().await;
                    for association in &name.associations {
                        associations.remove(association);
                    }
                }
                RecvType::Done => break Ok(()),
            }
        }
    } */
    pub fn new() -> Self {
        // let (send, recv) = channel::unbounded();
        Self {
            // send,
            // recv,
            // tasks: RefCell::default(),
            connections: RwLock::default(),
            associations: RwLock::default(),
            next_id: Cell::default(),
        }
    }
}

*/
>>>>>>> c3ab1bc (resync)
