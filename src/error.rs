use crate::{error::Error::*, service::Service};
use std::{
    cell::{BorrowError, BorrowMutError},
    fmt::{Debug, Display},
    rc::Rc,
    str::Utf8Error,
};

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Borrow(e) => write!(f, "{}", e),
            BorrowMut(e) => write!(f, "{}", e),
            InvalidQuery(string) => write!(f, "invalid query: {}", string),
            Binary(err) => write!(f, "{}", err),
            ServiceNotFound => f.write_str("The requested service was not found"),
            Utf8(err) => write!(f, "{}", err),
            ConditionTookTooLong(condition) => {
                write!(f, "condition {} took too long", condition.service_name)
            }
            Nix(err) => write!(f, "{}", err),
            Fmt(err) => write!(f, "{}", err),
            Toml(err) => write!(f, "{}", err),
            IO(err) => write!(f, "{}", err),
            ServicePathDoesNotExist => write!(f, "The specified path does not exist"),
            DependenciesUnsatisfied(dependencies) => {
                writeln!(
                    f,
                    "Failed to start service, the following dependencies are unsatisfied:"
                )?;
                for dependency in dependencies {
                    writeln!(f, "  {}", dependency)?;
                }
                Ok(())
            }
            ConditionFailed(condition) => write!(
                f,
                "Failed to start service, the following condition has failed: {}",
                condition.service_name
            ),
            ChildExitFail(code) => write!(f, "exit code {}", code),
            ChildFailedToWait => f.write_str("unable to check-in on child"),
            ChildIsGone => f.write_str("child has disappeared"),
            FailedToReadDirectory => f.write_str("failed to read service directory"),
            StderrIsGone => f.write_str("stderr has disappeared"),
            StdoutIsGone => f.write_str("stdout has disappeared"),
        }
    }
}
impl From<BorrowError> for Error {
    fn from(e: BorrowError) -> Self {
        Borrow(e)
    }
}
impl From<Utf8Error> for Error {
    fn from(e: Utf8Error) -> Self {
        Utf8(e)
    }
}
impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        IO(e)
    }
}
impl From<std::fmt::Error> for Error {
    fn from(e: std::fmt::Error) -> Self {
        Fmt(e)
    }
}
impl From<toml::de::Error> for Error {
    fn from(e: toml::de::Error) -> Self {
        Toml(e)
    }
}
impl From<nix::Error> for Error {
    fn from(e: nix::Error) -> Self {
        Nix(e)
    }
}
impl From<bincode::Error> for Error {
    fn from(e: bincode::Error) -> Self {
        Binary(e)
    }
}
impl From<BorrowMutError> for Error {
    fn from(e: BorrowMutError) -> Self {
        BorrowMut(e)
    }
}

pub(crate) type Result<T> = std::result::Result<T, Error>;
#[derive(Debug)]
pub(crate) enum Error {
    Borrow(BorrowError),
    BorrowMut(BorrowMutError),
    InvalidQuery(String),
    Binary(bincode::Error),
    ChildExitFail(i32),
    ChildFailedToWait,
    ChildIsGone,
    ConditionFailed(Rc<Service>),
    ConditionTookTooLong(Rc<Service>),
    DependenciesUnsatisfied(Vec<Rc<String>>),
    FailedToReadDirectory,
    Fmt(std::fmt::Error),
    IO(std::io::Error),
    Nix(nix::Error),
    ServiceNotFound,
    ServicePathDoesNotExist,
    StderrIsGone,
    StdoutIsGone,
    Toml(toml::de::Error),
    Utf8(Utf8Error),
}
