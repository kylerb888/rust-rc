<<<<<<< HEAD
use crate::{error::Error, listener::ServiceAction, service::Service};
use futures_util::{
    stream::{self, StreamExt},
    TryStreamExt,
};
use futures_micro::or;
use signal_hook_async_std::Signals;
use std::{
    array,
    fmt::Display,
    future,
    iter::FromIterator,
    ops::{Deref, DerefMut},
    path::Path,
    rc::Rc,
};

impl Extend<Rc<Service>> for Services {
    fn extend<T: IntoIterator<Item = Rc<Service>>>(&mut self, iter: T) {
        self.data.extend(iter);
    }
}
#[derive(Debug, Default)]
pub(crate) struct Services {
    pub(crate) data: Vec<Rc<Service>>,
}
impl FromIterator<Rc<Service>> for Services {
    fn from_iter<T: IntoIterator<Item = Rc<Service>>>(iter: T) -> Self {
        let mut c = Services::new();
        c.extend(iter);
        c
    }
}
impl FromIterator<Service> for Services {
    fn from_iter<T: IntoIterator<Item = Service>>(iter: T) -> Self {
        let mut c = Services::new();
        let iter = iter.into_iter().map(Rc::new);
        c.extend(iter);
        c
    }
}
impl Display for Services {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Available Services:")?;
        let max = self
            .iter()
            .map(|service| service.service_name.len())
            .max()
            .map_or(3, |f| f + 3);

        let (ok, err): (Vec<_>, Vec<_>) = self
            .iter()
            .map(|service| {
                let status = service.get_status();
                writeln!(
                    f,
                    "   {:<width$}|   {status:?}",
                    service.service_name,
                    status = status,
                    width = max
                )?;
                Ok(status)
            })
            .collect::<Result<Vec<_>, std::fmt::Error>>()?
            .into_iter()
            .partition(Result::is_ok);

        if err.is_empty() {
            writeln!(f, "OK: Supervising {} services", ok.len())?;
        } else {
            writeln!(
                f,
                "DEGRADED: Supervising {} services -- {} services have failed",
                ok.len(),
                err.len()
            )?;
        }
        Ok(())
    }
}

impl Services {
    pub(crate) fn cloned(&self) -> Self {
        self.iter().map(Rc::clone).collect()
    }
    pub(crate) async fn from_path(service_path: impl AsRef<Path>) -> Result<Self, Error> {
        smol::fs::read_dir(service_path)
            .await?
            .map_ok(|item| item.path())
            .map_ok(|p| async move {
                if Service::is_valid(&p).await {
                    let res = Service::from_path(&p).await;
                    res.inspect_err(|e| {
                        eprintln!("Failed to load service: '{}': {}", p.display(), e)
                    });
                    Ok(res.ok())
                } else {
                    Ok(None)
                }
            })
            .err_into()
            .try_buffer_unordered(usize::MAX)
            .map(Result::transpose)
            .filter_map(future::ready)
            .map_ok(Rc::new)
            .try_collect::<Services>()
            .await
    }
    pub(crate) fn create_ttys() -> impl Iterator<Item = Rc<Service>> {
        (1..=7)
            .map(|num| {
                let tty = format!("tty{}", num);
                Service {
                    cmd: "agetty".into(),
                    service_name: Rc::new(tty.to_owned()),
                    args: Box::new([tty, "linux".into(), "--noclear".into()]),
                    keep_alive: true,
                    no_log: true,
                    priority: -2,
                    ..Default::default()
                }
            })
            .map(Rc::new)
    }
    pub(crate) fn new() -> Self {
        Self { data: Vec::new() }
    }
    pub(crate) async fn start(&self) -> Result<(), Error> {
        let res = stream::iter(&self.data)
            .map(Rc::deref)
            .map(Service::start)
            .buffer_unordered(usize::MAX)
            .filter(|i| future::ready(i.is_err()))
            .next()
            .await
            .unwrap_or(Ok(()));
        res
    }
    pub(crate) async fn stop(&self) -> Result<(), Error> {
        let stream = stream::iter(&self.data)
            .map(Rc::deref)
            .map(Service::start)
            .buffer_unordered(usize::MAX)
            .filter(|i| future::ready(i.is_err()))
            .next()
            .await
            .unwrap_or(Ok(()));
        stream
    }
    pub(crate) async fn track(&self) -> Result<(), Error> {
        let mut stream = stream::iter(&self.data)
            .map(Rc::deref)
            .map(Service::track)
            .buffer_unordered(usize::MAX)
            .filter(|i| future::ready(i.is_err()));
        let err = stream.next().await.unwrap_or(Ok(()));
        stream.for_each(|_| future::ready(())).await;

        err
    }
    pub(crate) fn get_conditions(&self) -> Services {
        let mut test: Services = self
            .data
            .iter()
            .flat_map(|i| i.wait_on.borrow().cloned().data)
            .collect();
        test.sort();
        test.dedup();
        test
    }
    pub(crate) async fn check_conditions(&self) -> Result<(), Error> {
        let conditions = Services::from_path("/etc/rust-rc.d/conditions.d").await?;
        self.data
            .iter()
            .map(|service| service.check_conditions(&conditions))
            .filter(Result::is_err)
            .next()
            .unwrap_or(Ok(()))
    }
    pub(crate) async fn supervise(&mut self) -> Result<ServiceAction, Error> {
        println!("Supervising ... ");

        loop {
            let track = self.track();
            let listener = ServiceAction::listen(&self);
            let res = or!(
                async {
                    track.await?;
                    panic!();
                },
                async {
                    let res = match Services::await_signal().await {
                        Some(1) => ServiceAction::Reboot,
                        Some(15) => ServiceAction::Poweroff,
                        _ => ServiceAction::Continue,
                    };
                    Ok(res)
                },
                listener
            )
            .await?;
            match res {
                ServiceAction::ChangeServices(services) => *self = services,
                ServiceAction::Poweroff | ServiceAction::Reboot => return Ok(res),
                _ => {}
            }
        }
    }
    pub(crate) async fn await_signal() -> Option<i32> {
        Signals::new(array::IntoIter::new([1, 15]))
            .unwrap()
            .next()
            .await
    }
    pub(crate) fn check_all_dependencies(&mut self) -> Result<(), Vec<ServiceResult<(), Error>>> {
        let mut cloned = self.cloned();
        let mut errs = false;
        let mut err_out = Vec::new();
        loop {
            self.retain(|service| {
                let unsatisfied_dependencies = service.get_unsatisfied_dependencies(&cloned);
                if let Err(err) = unsatisfied_dependencies {
                    eprintln!("Failed to start {}: {}", service.service_name, err);
                    errs = true;
                    err_out.push(ServiceResult {
                        name: Rc::clone(&service.service_name),
                        result: Err(err),
                    });
                    false
                } else {
                    true
                }
            });
            if !errs {
                break;
            } else {
                cloned = self.cloned();
                errs = false;
            }
        }
        if err_out.is_empty() {
            Ok(())
        } else {
            Err(err_out)
        }
    }
}
impl Deref for Services {
    type Target = Vec<Rc<Service>>;
=======
use crate::{
    listener::{ServiceAlreadyStartedError, ServiceNotFoundError},
    service::SingleThreadMutex,
};
use std::{borrow::Cow, collections::HashMap, env, rc::Rc};

use crate::{
    listener::{Listener, ServiceAction},
    service::{CommandBase, Service, WrappedService},
};
use futures_util::{future, stream::FuturesUnordered, FutureExt, StreamExt, TryStreamExt, Future};
use indexmap::IndexSet;
use nix::unistd::Uid;
use rust_rc_ipc_structures::{DynResult, ServiceSummary};
use std::ops::Deref;
use tokio::{
    fs,
    io::AsyncWrite,
    select,
    sync::{RwLock, RwLockReadGuard},
    try_join,
};
use tokio_stream::wrappers::ReadDirStream;

#[derive(Debug, Default)]
pub struct SharedServices {
    data: RwLock<IndexSet<WrappedService>>,
    units: RwLock<HashMap<String, Vec<String>>>,
}

impl std::ops::Deref for SharedServices {
    type Target = RwLock<IndexSet<WrappedService>>;
>>>>>>> c3ab1bc (resync)

    fn deref(&self) -> &Self::Target {
        &self.data
    }
}
<<<<<<< HEAD
impl DerefMut for Services {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.data
    }
}
impl IntoIterator for Services {
    type Item = Rc<Service>;

    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.data.into_iter()
    }
}
use crate::listener::ServiceResult;

pub(crate) trait InspectErr<E, C: Fn(&E) -> ()> {
    /// Inspect error in result (if any)
    /// Example:
    /// ```
    /// let err: Result<(), &str> = Err("this is an error");
    /// err.inspect_err(|e| eprintln!("Received error: {}", e));
    /// ```
    fn inspect_err(&self, op: C);
}
impl<T, E, C: Fn(&E) -> ()> InspectErr<E, C> for Result<T, E> {
    fn inspect_err(&self, op: C) {
        if let Err(err) = self {
            op(err);
=======
impl FromIterator<Service> for SharedServices {
    fn from_iter<T: IntoIterator<Item = Service>>(iter: T) -> Self {
        let mut def = Self::default();
        def.extend(iter);
        def
    }
}
impl Extend<Service> for SharedServices {
    fn extend<T: IntoIterator<Item = Service>>(&mut self, iter: T) {
        for service in iter {
            self.try_push(service).unwrap();
        }
    }
}
impl SharedServices { 
    pub async fn as_futures_with<F, Fut>(&self, op: F) -> FuturesUnordered<Fut>
    where F: Fn(Rc<Service>) -> Fut
    {
        self.data
            .write()
            .await
            .iter()
            .map(WrappedService::clone_inner)
            .map(op)
            .collect()

    }
    pub async fn get_providers_of_unit(&self, unit: &str) -> Vec<String> {
        self.units
            .read()
            .await
            .get(unit)
            .cloned()
            .unwrap_or_default()
    }
    pub fn try_push(&self, sv: Service) -> DynResult<()> {
        let mut write_lock = self.units.try_write()?;
        for unit in &sv.provides_units {
            write_lock
                .entry(unit.to_owned())
                .or_insert_with(Vec::new)
                .push(sv.service_name.to_owned());
            eprintln!("service {} now provides unit {unit}", sv.service_name);
        }
        drop(write_lock);
        self.data.try_write()?.insert(WrappedService::new(sv));
        Ok(())
    }
    pub async fn push(&self, sv: Service) {
        let mut write_lock = self.units.write().await;
        for unit in &sv.provides_units {
            write_lock
                .entry(unit.to_owned())
                .or_insert_with(Vec::new)
                .push(sv.service_name.to_owned());
            // eprintln!("service {} now provides unit {unit}", sv.service_name);
        }
        drop(write_lock);
        self.data.write().await.insert(WrappedService::new(sv));
    }
    pub fn new() -> Self {
        Self {
            data: RwLock::default(),
            units: RwLock::default(),
        }
    }
    pub async fn stream_state(
        &self,
        names: &[impl AsRef<str>],
        connection: &SingleThreadMutex<impl AsyncWrite + Unpin>,
    ) -> DynResult<()> {
        let read_guard = self.read().await;
        let collect = names
            .iter()
            .filter_map(|i| read_guard.get(i.as_ref()))
            .map(WrappedService::clone_inner)
            .map(|service| async move {
                try_join!(
                    service.stream_status(connection),
                    service.stream_state(connection)
                )
                .map(drop)
            })
            .collect::<FuturesUnordered<_>>();
        drop(read_guard);
        collect.try_for_each(future::ok).await
    }
    pub fn stop<'a>(&'a self, names: &'a [impl AsRef<str>], allow_restart: bool) -> impl Future<Output=Vec<String>> + 'a {
        names
            .iter()
            .map(|name| async move {
                let name = name.as_ref();
                let found = self
                    .read()
                    .await
                    .get(name)
                    .map(WrappedService::clone_inner)
                    .ok_or_else(|| ServiceNotFoundError::new(name.to_owned()))?;
                found.stop(allow_restart, true).await?;

                DynResult::Ok(name)
            })
            .collect::<FuturesUnordered<_>>()
            .filter_map(|res| future::ready(res.err()))
            .map(|item| item.to_string())
            .collect()
    }
    pub async fn start(
        &self,
        names: &[impl AsRef<str>],
        services_base_path: &str,
    ) -> (Vec<String>, Vec<String>) {
        enum GetType {
            Name(String),
            Service(Service),
        }
        let (ok, err) = names
            .iter()
            .map(|name| async move {
                let name = name.as_ref();
                match self.look_up(name).await {
                    Some(get) if get.is_running() => DynResult::Err(
                        ServiceAlreadyStartedError::new(get.service_name.to_owned()).into(),
                    ),
                    Some(get) => {
                        get.clean()?;
                        Ok(GetType::Name(name.to_owned()))
                    }
                    None => {
                        let get = Service::from_path(format!(
                            "{services_base_path}/services.d/{name}.toml"
                        ))
                        .await?;
                        Ok(GetType::Service(get))
                    }
                }
            })
            .collect::<FuturesUnordered<_>>()
            .map_err(|i| i.to_string())
            .fold((Vec::new(), Vec::new()), |(mut ok, mut err), x| {
                match x {
                    Ok(get) => ok.push(get),
                    Err(get) => err.push(get),
                };
                future::ready((ok, err))
            })
            .await;
        let names = ok
            .iter()
            .map(|sv| match sv {
                GetType::Name(name) => name.to_owned(),
                GetType::Service(service) => service.service_name.to_owned(),
            })
            .collect();
        self.extend_async(ok.into_iter().filter_map(|sv| match sv {
            GetType::Service(service) => Some(service),
            _ => None,
        }))
        .await;
        (names, err)
    }
    pub async fn summary(&self) -> Vec<ServiceSummary> {
        self.read()
            .await
            .iter()
            .map(WrappedService::deref)
            .map(Rc::deref)
            .map(Service::summary)
            .collect()
    }
    pub async fn stream_summary(
        &self,
        connection: &SingleThreadMutex<impl AsyncWrite + Unpin>,
    ) -> DynResult<()> {
        self.get()
            .await
            .into_iter()
            .map(|service| (service, &connection))
            .map(|(service, connection)| async move { service.stream_state(connection).await })
            .collect::<FuturesUnordered<_>>()
            .try_for_each(future::ok)
            .await
    }
    pub async fn extend_async(&self, iter: impl IntoIterator<Item = Service>) {
        let mut units = self.units.write().await;
        let mut services = self.data.write().await;
        for sv in iter {
            sv.provides_units.iter().cloned().for_each(|unit| {
                units
                    .entry(unit)
                    .or_insert_with(Vec::new)
                    .push(sv.service_name.to_owned())
            });
            services.insert(WrappedService::new(sv));
        }
    }
    pub async fn look_up(&self, name: &str) -> Option<WrappedService> {
        self.data.read().await.get(name).cloned()
    }
    /// As long as this guard is active, the contents of the IndexSet will be write-locked until this and all other read locks are dropped.
    pub async fn read_inner(&self) -> RwLockReadGuard<'_, IndexSet<WrappedService>> {
        self.data.read().await
    }
    pub async fn get(&self) -> Vec<Rc<Service>> {
        self.data
            .read()
            .await
            .iter()
            .map(WrappedService::clone_inner)
            .collect()
    }
}

#[derive(Debug)]
pub struct Services {
    data: SharedServices,
    listener: Listener,
    base_path: Cow<'static, str>,
}
impl Services {
    fn create_ttys() -> impl Iterator<Item = Service> {
        (1..=7).map(|num| {
            let name = format!("tty{num}");
            // Something odd is that when building LFS
            // wrapping agetty in setsid is required
            // in order for job control to work in shells.
            // I'm not sure why this is the case, but it
            // isn't required when using any other
            // distribution. However, the other difference
            // is that this project was compiled with musl
            // on that machine, maybe that makes the difference?
            Service {
                command: CommandBase {
                    cmd: "setsid".into(),
                    args: vec![
                        String::from("agetty"),
                        name.to_owned(),
                        String::from("linux"),
                        String::from("--noclear"),
                    ],
                },
                units: vec![String::from("default")],
                service_name: name,
                keep_alive: true,
                ..Default::default()
            }
        })
    }
    pub async fn from_default_path() -> DynResult<Self> {
        let (base_path, socket_path, is_root) = if Uid::current().is_root() {
            (
                Cow::Borrowed("/etc/rust-rc.d"),
                Cow::Borrowed("/run/rust-rc_2.sock"),
                true,
            )
        } else {
            (
                Cow::Owned(format!("{}/rust-rc.d", env::var("HOME")?)),
                Cow::Owned(format!("{}/rust-rc_2.sock", env::var("XDG_RUNTIME_DIR")?)),
                false,
            )
        };
        let get = Self::from_path(base_path, socket_path).await?;
        if is_root {
            get.data
                .write()
                .await
                .extend(Services::create_ttys().map(WrappedService::new))
        }
        Ok(get)
    }
    pub async fn stop(&self, with_stop_commands: bool) -> DynResult<()> {
        self.data
            .get()
            .await
            .iter()
            .filter(|sv| sv.is_running())
            .map(|sv| sv.stop(false, with_stop_commands))
            .collect::<FuturesUnordered<_>>()
            .try_for_each(future::ok)
            .await
    }
    pub async fn from_path(
        base_path: Cow<'static, str>,
        socket_path: Cow<'static, str>,
    ) -> DynResult<Self> {
        let get = dbg!(fs::read_dir(dbg!(format!("{base_path}/services.d/enabled.d")))
            .await)
            .map(ReadDirStream::new)?
            .map_ok(|i| i.path())
            .try_filter(|i| future::ready(Service::is_valid(i)))
            .err_into()
            .map_ok(Service::from_path)
            .try_buffer_unordered(usize::MAX)
            .filter_map(|i| {
                let get = i.map_or_else(
                    |err| {
                        eprintln!("Failed to load service: {err}");
                        None
                    },
                    Some,
                );
                future::ready(get)
            })
            .collect()
            .await;
        Ok(Self {
            data: get,
            listener: Listener::new(&socket_path)?,
            base_path,
        })
    }
    pub async fn supervise(&mut self) {
        let mut supervise_futs = self
            .data
            .get()
            .await
            .into_iter()
            .map(|sv| sv.supervise(&self.data))
            .collect::<FuturesUnordered<_>>();
        let mut queries = self.listener.queries(&self.data, &self.base_path);
        loop {
            let current = supervise_futs
                .by_ref()
                .map(Result::err)
                .filter_map(future::ready)
                .for_each(|err| {
                    eprintln!("Service failed: {err}");
                    future::ready(())
                })
                .then(|_| async {
                    eprintln!("There is nothing to do.");
                    () = future::pending().await;
                    unreachable!()
                });

            let action = select! {
                unreachable = current => unreachable,
                res = queries.next() => res.unwrap(),
            };
            let read = self.data.read_inner().await;
            if let ServiceAction::Start(services) = action {
                for service in services {
                    if let Some(found) = read.get(&*service) {
                        supervise_futs.push(found.clone_inner().supervise(&self.data));
                    }
                }
            }
>>>>>>> c3ab1bc (resync)
        }
    }
}
